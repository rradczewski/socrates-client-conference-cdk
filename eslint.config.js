const parser = require('@typescript-eslint/parser');
const typescript = require('@typescript-eslint/eslint-plugin');
const globals = require('globals');

module.exports = {
  files: ['src/**/*.ts', 'src/**/*.tsx'],
  ignores: [
    'src/app/**/*',
    'src/**/*.js',
    'src/**/*.map',
    'src/**/serviceWorker.ts',
  ],
  languageOptions: {
    parser,
    parserOptions: {
      'ecmaVersion': 2022
    },
    globals: {
      ...globals.browser,
      node: true,
      jest: true
    },
  },
  plugins: {
    '@typescript-eslint': typescript,
    configs: typescript.configs.recommended
  },
  rules: {
    '@typescript-eslint/indent': ['warn', 2],
    '@typescript-eslint/explicit-function-return-type': ['warn', { 'allowExpressions': true }],
    'quotes': ['warn', 'single'],
    'semi': ['warn', 'never'],
    'no-extra-semi': 'warn',
    'no-unexpected-multiline': 'warn',
    '@typescript-eslint/explicit-member-accessibility': 0,
    '@typescript-eslint/member-delimiter-style': [
      'warn',
      {
        'multiline': {
          'delimiter': 'none',
          'requireLast': false
        },
        'singleline': {
          'delimiter': 'comma',
          'requireLast': false
        }
      }
    ]
  }
};
