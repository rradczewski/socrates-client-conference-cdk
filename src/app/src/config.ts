const UI_ENDPOINT: string = 'https://conf.socrates-conference.de';
const WEB_CLIENT_ENDPOINT: string = 'https://www.socrates-conference.de';
const API_ENDPOINT: string = 'https://api.socrates-conference.de';
const AUTH_ENDPOINT: string =
  'socrates-conf.auth.eu-central-1.amazoncognito.com';
export const config = {
  conference: 'socrates24',
  logoPath: WEB_CLIENT_ENDPOINT,
  apiEndpoints: {
    newsletters: API_ENDPOINT,
    sponsors: API_ENDPOINT,
    slots: API_ENDPOINT,
    subscribers: API_ENDPOINT,
    templates: API_ENDPOINT,
    conferences: API_ENDPOINT,
    applicants: API_ENDPOINT,
    attendees: API_ENDPOINT,
    booking: API_ENDPOINT,
    profile: API_ENDPOINT,
    rooms: API_ENDPOINT,
  },
  minimumSponsoringAttendees: 190,
  auth: {
    region: 'eu-central-1',
    userPoolId: 'eu-central-1_DkIUYTQCl',
    userPoolWebClientId: '7pfo6an5uq9t6ah0n8l5i6o6m0',
    oauth: {
      domain: AUTH_ENDPOINT,
      scope: ['email', 'openid'],
      redirectSignIn: UI_ENDPOINT,
      redirectSignOut: UI_ENDPOINT,
      responseType: 'token',
    },
  },
};
