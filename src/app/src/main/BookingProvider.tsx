import { Stats } from './types';
import { createContext, PropsWithChildren, useContext } from 'react';
import { BookingApi } from './api/booking';

export type BookingContext = {
  getStats: (conference: string) => Promise<Stats>;
};

const NIL_STATS: Stats = {
  attendeeCount: 0,
  sponsoringAmount: '0€',
  expensesAmount: '0€',
  bookedPackages: { conference: 0 },
};

const DEFAULT_CONTEXT: BookingContext = {
  getStats: (_: string) => {
    console.log('not getting');
    return Promise.resolve(NIL_STATS);
  },
};

const bookingContext = createContext(DEFAULT_CONTEXT);
export const useBooking = () => useContext(bookingContext);

export type BookingProviderProps = {
  bookingApi: BookingApi;
};

export function BookingProvider({
  bookingApi,
  children,
}: PropsWithChildren<BookingProviderProps>) {
  const getStats = async (conference: string) => {
    return await bookingApi.getStats(conference);
  };

  const context = {
    getStats,
  };
  return (
    <bookingContext.Provider value={context}>
      {children}
    </bookingContext.Provider>
  );
}
