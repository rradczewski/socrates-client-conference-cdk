import axios from 'axios';
import { Attendee, AttendeeInfo, Booking, Identifier } from '../types';
import { config } from '../../config';
import {
  InternalErrorException,
  NetworkException,
  NotFoundException,
  UnauthorizedException,
} from './error';

const profileApi: string = config.apiEndpoints.profile;
const profileEndpoint: string = `${profileApi}/profile`;

export type ProfileApi = {
  cancelParticipation: (conference: Identifier, email: string) => Promise<void>;
  getProfile: (conference: Identifier, email: string) => Promise<AttendeeInfo>;
  updateProfile: (
    attendee: Attendee,
    booking: Booking,
  ) => Promise<string | undefined>;
};

const getProfile = async (conference: Identifier, email: string) =>
  await axios.get(`${profileEndpoint}/${conference}/${email}`).catch((e) => {
    console.error('Error while updating profile:', e);
    if (e.message.endsWith('401')) {
      throw new UnauthorizedException();
    } else if (e.message.endsWith('404')) {
      throw new NotFoundException();
    } else if (
      e.message.endsWith('500') ||
      e.message === 'Internal Server Error'
    ) {
      throw new InternalErrorException();
    } else if (e.message === 'Network Error') {
      throw new NetworkException();
    }
  });

const putProfile = async (attendee: Attendee, booking: Booking) =>
  await axios
    .put(`${profileEndpoint}/${attendee.email}`, {
      attendee,
      booking,
    })
    .catch((e) => {
      console.error('Error while updating profile:', e);
      if (e.message.endsWith('401')) {
        throw new UnauthorizedException();
      } else if (e.message.endsWith('404')) {
        throw new NotFoundException();
      } else if (e.message.endsWith('500')) {
        throw new InternalErrorException();
      } else if (e.message === 'Network Error') {
        throw new NetworkException();
      }
    });

const deleteProfile = async (conference: Identifier, email: string) =>
  await axios.delete(`${profileEndpoint}/${conference}/${email}`).catch((e) => {
    console.error('Error while updating profile:', e);
    if (e.message.endsWith('401')) {
      throw new UnauthorizedException();
    } else if (e.message.endsWith('404')) {
      throw new NotFoundException();
    } else if (e.message.endsWith('500')) {
      throw new InternalErrorException();
    } else if (e.message === 'Network Error') {
      throw new NetworkException();
    }
  });

export class Profile implements ProfileApi {
  public getProfile = async (conference: Identifier, email: string) => {
    const result = await getProfile(conference, email);
    if (result) {
      if (result.status === 401) {
        throw new UnauthorizedException();
      } else if (result.status === 404) {
        throw new NotFoundException();
      } else if (result.status >= 400) {
        throw new InternalErrorException();
      }
    }
    return result?.data as AttendeeInfo;
  };

  public updateProfile = async (attendee: Attendee, booking: Booking) => {
    const result = await putProfile(attendee, booking);
    if (result) {
      if (result.status === 200) {
        return result.data?.message;
      }
      if (result.status === 401) {
        throw new UnauthorizedException();
      } else if (result.status === 404) {
        throw new NotFoundException();
      } else if (result.status >= 400) {
        throw new InternalErrorException();
      }
    }
  };

  public cancelParticipation = async (
    conference: Identifier,
    email: string,
  ) => {
    const result = await deleteProfile(conference, email);
    if (result) {
      if (result.status === 401) {
        throw new UnauthorizedException();
      } else if (result.status === 404) {
        throw new NotFoundException();
      } else if (result.status >= 400) {
        throw new InternalErrorException();
      }
    }
  };
}
