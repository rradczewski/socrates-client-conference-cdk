class Exception extends Error {
  public type?: string;
}
export class InternalErrorException extends Exception {
  constructor() {
    super();
    this.type = 'InternalErrorException';
  }
}

export class NetworkException extends Exception {
  constructor() {
    super();
    this.type = 'NetworkException';
  }
}

export class BadRequestException extends Exception {
  constructor() {
    super();
    this.type = 'BadRequestException';
  }
}

export class NotFoundException extends Exception {
  constructor() {
    super();
    this.type = 'NotFoundException';
  }
}

export class UnauthorizedException extends Exception {
  constructor() {
    super();
    this.type = 'UnauthorizedException';
  }
}

export class AlreadyRegisteredException extends Exception {
  constructor() {
    super();
    this.type = 'AlreadyRegisteredException';
  }
}
