import axios from 'axios';
import { Attendee } from '../types';
import { config } from '../../config';
import {
  AlreadyRegisteredException,
  BadRequestException,
  InternalErrorException,
  NotFoundException,
} from './error';

const attendeesApi: string = config.apiEndpoints.attendees;
const attendeesEndpoint: string = `${attendeesApi}/attendees`;

export type AttendeesApi = {
  confirm: (email: string, confirmKey: string) => Promise<void>;
};

export class Attendees implements AttendeesApi {
  private _attendee: Attendee | undefined;

  public constructor() {
    this._attendee = undefined;
  }

  public confirm = async (email: string, key: string) => {
    await axios
      .put(attendeesEndpoint + '/confirm', {
        conferenceId: config.conference,
        email,
        key,
      })
      .catch((e) => {
        console.error('Error while confirming attendee:', e);
        if (e.message.endsWith('409')) {
          throw new AlreadyRegisteredException();
        } else if (e.message.endsWith('400')) {
          throw new BadRequestException();
        } else if (e.message.endsWith('404')) {
          throw new NotFoundException();
        } else {
          throw new InternalErrorException();
        }
      });
  };
}
