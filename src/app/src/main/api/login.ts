import { Auth } from 'aws-amplify';
import axios from 'axios';

const handleSuccess = (info: any) => {
  axios.defaults.headers.common['Authorization'] =
    `Bearer ${info.signInUserSession.idToken.jwtToken}`;
};

export const signIn = (username: string, password: string) => {
  return Auth.signIn(username, password).then((info) => {
    handleSuccess(info);
    return info;
  });
};

export const signOut = () => {
  return Auth.signOut();
};
