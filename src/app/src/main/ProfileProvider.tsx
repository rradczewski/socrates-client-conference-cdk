import React, {
  createContext,
  PropsWithChildren,
  useContext,
  useState,
} from 'react';
import { Attendee, AttendeeInfo, Booking, Identifier } from './types';
import { ProfileApi } from './api/profile';
import { useAuth } from './AuthProvider';

type ProfileContext = {
  cancelParticipation: (conference: Identifier) => Promise<void>;
  getProfile: (conference: Identifier) => Promise<AttendeeInfo | undefined>;
  updateProfile: (
    attendee: Attendee,
    booking: Booking,
  ) => Promise<string | undefined>;
};

const DEFAULT_CONTEXT: ProfileContext = {
  getProfile: (): Promise<AttendeeInfo | undefined> => {
    console.log('not getting ');
    return Promise.resolve(undefined);
  },
  cancelParticipation: (): Promise<void> => {
    console.log('not canceling ');
    return Promise.resolve();
  },
  updateProfile: (attendee): Promise<string | undefined> => {
    console.log('not updating:', attendee);
    return Promise.resolve(undefined);
  },
};

const profileContext = createContext(DEFAULT_CONTEXT);
export const useProfile = () => useContext(profileContext);

type ProvideProfilesProps = {
  profileApi: ProfileApi;
};

export const ProfileProvider = ({
  profileApi,
  children,
}: PropsWithChildren<ProvideProfilesProps>) => {
  const authContext = useAuth();
  const email = authContext.email;
  const [attendee, setAttendee] = useState<AttendeeInfo>();

  const getProfile = async (
    conference: Identifier,
  ): Promise<AttendeeInfo | undefined> => {
    if (!attendee) {
      await profileApi
        .getProfile(conference, email)
        .then(setAttendee)
        .catch((e) => {
          throw e;
        });
    }
    return attendee;
  };

  const updateProfile = async (attendee: Attendee, booking: Booking) => {
    return await profileApi.updateProfile(attendee, booking).then((result) => {
      setAttendee({ attendee, booking });
      return result;
    });
  };

  const cancelParticipation = async (conference: Identifier): Promise<void> => {
    await profileApi.cancelParticipation(conference, email).catch((e) => {
      throw e;
    });
  };

  const context = {
    getProfile,
    updateProfile,
    cancelParticipation,
  };

  return (
    <profileContext.Provider value={context}>
      {children}
    </profileContext.Provider>
  );
};
