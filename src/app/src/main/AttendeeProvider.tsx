import React, { createContext, PropsWithChildren, useContext } from 'react';
import { AttendeesApi } from './api/attendees';

type AttendeeContext = {
  confirm: (email: string, confirmKey: string) => Promise<void>;
};

const DEFAULT_CONTEXT: AttendeeContext = {
  confirm: () => {
    console.log('not confirming');
    return Promise.resolve();
  },
};

const attendeeContext = createContext(DEFAULT_CONTEXT);
export const useAttendee = () => useContext(attendeeContext);

type ProvideAttendeesProps = {
  attendeesApi: AttendeesApi;
};

export const AttendeeProvider = ({
  attendeesApi,
  children,
}: PropsWithChildren<ProvideAttendeesProps>) => {
  const confirm = async (email: string, confirmKey: string) => {
    await attendeesApi.confirm(email, confirmKey);
  };

  const context = {
    confirm,
  };

  return (
    <attendeeContext.Provider value={context}>
      {children}
    </attendeeContext.Provider>
  );
};
