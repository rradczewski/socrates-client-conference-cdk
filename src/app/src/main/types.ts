export type UUID = string;
export type Identifier = string;

export type Iterable = { [key: string]: any };

export type TableRow = { index?: number };

export type Entity = {
  id: UUID;
};

export type RoomType = Iterable & {
  type: RoomTypeName;
  description: string;
  pricePerNight: string;
  beds: number;
  count: number;
};

export type FlatFee = Iterable & {
  type: Package;
  description: string;
  price: string;
  excludeFromSponsoring?: boolean;
};

export enum ConferenceState {
  PREPARATION = 'PREPARATION',
  REGISTRATION = 'REGISTRATION',
  IN_PROGRESS = 'IN_PROGRESS',
  CLEANUP = 'CLEANUP',
  CONCLUDED = 'CONCLUDED',
}

export type Conference = Iterable &
  TableRow &
  Entity & {
    byline?: string;
    diversityRatio?: number;
    endDate?: string;
    flatFees?: FlatFee[];
    location: string;
    maxSeatsPerSponsor?: number;
    roomTypes?: RoomType[];
    startDate?: string;
    state?: ConferenceState;
    title: string;
    year: number;
  };

export type Day = 'Wed' | 'Thu' | 'Fri' | 'Sat' | 'Sun';

export type Package = (Identifier & 'conference') | 'training' | 'workshops';

export type RoomTypeName =
  | (Identifier & 'none')
  | 'single'
  | 'double-shared'
  | 'junior-double'
  | 'junior-double-shared';

export type Address = {
  firstname?: string;
  lastname?: string;
  address1?: string;
  address2?: string;
  postal?: string;
  city?: string;
  country?: string;
};

export type BillingAddress = Address & {
  company?: string;
  vat?: string;
};

export type Attendee = TableRow &
  Entity & {
    conferenceId: UUID;
    email: string;
    personalAddress?: Address;
    billingAddress?: BillingAddress;
    billingAddressIsPersonalAddress: boolean;
    nickname: string;
    reason: string;
    pronouns?: string;
    social?: string;
    badgename?: string;
    diversitySelected: string;
    privacy: boolean;
  };

export type Price = string;

export type Stats = {
  attendeeCount: number;
  sponsoringAmount: Price;
  expensesAmount: Price;
  bookedPackages: Record<string, number>;
};

export type RoomOccupancy = {
  roomType: RoomTypeName;
  email: string;
  roommate?: string;
  family: boolean;
  daysSelected: Day[];
};

export type ChildcareRequirements = {
  children: number;
  details: string;
};

export type Booking = {
  email: string;
  room: RoomOccupancy;
  packages: Package[];
  swag?: boolean;
  swagCut?: string;
  swagSize?: string;
  dietary?: string;
  childcare?: ChildcareRequirements;
};

export type AttendeeInfo = { attendee: Attendee; booking: Booking };
