import React, { ChangeEvent, useState } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import { LoginCard } from './LoginCard';
import { useAuth } from '../main/AuthProvider';

export const LoginScreen = () => {
  const history = useHistory();
  const location = useLocation<any>();
  const auth = useAuth();

  let { from } = location.state || { from: { pathname: '/' } };

  const login = async (username: string, password: string) =>
    await auth.signIn(username, password).then(() => {
      history.replace(from);
    });

  const [error, setError] = useState(false);
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const handleFailure = (e: Error) => {
    setError(true);
    setTimeout(() => setError(false), 500);
    console.log('Error while logging in:', e);
  };
  const handleUsernameChange = (event: ChangeEvent<HTMLInputElement>) =>
    setUsername(event?.target?.value || '');
  const handlePasswordChange = (event: ChangeEvent<HTMLInputElement>) =>
    setPassword(event?.target?.value || '');
  const handleClear = () => {
    setUsername('');
    setPassword('');
    setError(false);
  };
  const handleLogin = async () => {
    await login(username, password).catch((e) => {
      handleFailure(e);
    });
  };
  return (
    <div className="row mt-4 mb-4">
      <div className="col-12 col-sm-10 col-md-8 col-lg-4 mx-auto">
        <LoginCard
          error={error}
          username={username}
          password={password}
          onUsernameChange={handleUsernameChange}
          onPasswordChange={handlePasswordChange}
          onClear={handleClear}
          onSubmit={handleLogin}
        />
      </div>
    </div>
  );
};
