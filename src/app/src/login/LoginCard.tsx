import React, { FormEvent, KeyboardEventHandler } from 'react';
import { Button, Form } from 'react-bootstrap';
import { ChangeHandler, FormSubmitHandler } from '../form/events';
import { InputField } from '../form/InputField';
import './LoginCard.scss';
import { config } from '../config';

type UsernameFieldProps = {
  className: string;
  value: string;
  onKeyPress?: KeyboardEventHandler<HTMLInputElement>;
  onChange: ChangeHandler<HTMLInputElement>;
};
const UsernameField = (props: UsernameFieldProps) => (
  <InputField
    className={props.className}
    value={props.value}
    label="Email"
    placeholder="Email"
    onChange={props.onChange}
  />
);

type PasswordFieldProps = {
  value: string;
  onKeyPress?: KeyboardEventHandler<HTMLInputElement>;
  onChange: ChangeHandler<HTMLInputElement>;
};
const PasswordField = (props: PasswordFieldProps) => (
  <InputField
    className="mb-0"
    value={props.value}
    label="Password"
    placeholder="••••••••"
    fieldType="password"
    onKeyPress={props.onKeyPress}
    onChange={props.onChange}
  />
);

type Props = {
  error: boolean;
  username: string;
  password: string;
  onClear: () => void;
  onUsernameChange: ChangeHandler<HTMLInputElement>;
  onPasswordChange: ChangeHandler<HTMLInputElement>;
  onSubmit: FormSubmitHandler;
};

export const LoginCard = (props: Props) => {
  const errorClass = props.error ? 'error border-danger ' : 'border-primary ';
  const handleKeyPress: KeyboardEventHandler<HTMLInputElement> = async (
    event,
  ) => {
    event.persist();
    if (event.key === 'Enter') {
      await props.onSubmit({} as unknown as FormEvent);
    }
  };
  return (
    <div className={errorClass + 'card login shadow-lg mt-2'}>
      <Form>
        <div className="card-header border-primary bg-primary b-0">
          <img
            className="logo img-fluid"
            alt="SoCraTes logo"
            src="https://raw.githubusercontent.com/softwerkskammer/softwerkskammer-logos/master/SoCraTes/2024/SoCraTesWappen_2024.png"
          />
          <h5 className="login-header text-light mb-0">Please log in:</h5>
        </div>
        <div className="card-body">
          <UsernameField
            className="mb-3"
            value={props.username}
            onChange={props.onUsernameChange}
            onKeyPress={handleKeyPress}
          />
          <PasswordField
            value={props.password}
            onChange={props.onPasswordChange}
            onKeyPress={handleKeyPress}
          />
          <div className="mt-3 mb-0">
            <p className="m-0 p-0 ps-1">
              First time logging in? Please{' '}
              <a
                href={`https://signup.socrates-conference.de/oauth2/authorize?client_id=${config.auth.userPoolWebClientId}&response_type=code&scope=email+openid&redirect_uri=https%3A%2F%2Fconf.socrates-conference.de`}
                title="Go to initial password setup"
              >
                replace your temporary password
              </a>{' '}
              before you continue.
            </p>
          </div>
        </div>
        <div className="card-footer text-end">
          <Button
            onClick={props.onClear}
            className="btn-outline-dark btn-light me-2"
          >
            Clear
          </Button>
          <Button onClick={props.onSubmit} className="btn-primary">
            Login
          </Button>
        </div>
      </Form>
    </div>
  );
};
