import { ChangeEvent, FormEvent, KeyboardEventHandler } from 'react';

export type ChangeHandler<T extends Element> = (event: ChangeEvent<T>) => void;
export type FormSubmitHandler = (event: FormEvent) => Promise<void>;
export type KeyPressHandler<T extends HtmlInputElement> =
  KeyboardEventHandler<T>;
