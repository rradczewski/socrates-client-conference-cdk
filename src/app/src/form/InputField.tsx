import { FormControl, InputGroup } from 'react-bootstrap';
import React from 'react';
import { ChangeHandler, KeyPressHandler } from './events';
import './InputField.scss';

export type InputFieldType = 'text' | 'password' | 'file';

type InputFieldProps = {
  label: string;
  disabled?: boolean;
  required?: boolean;
  value: string;
  placeholder: string;
  className?: string;
  fieldType?: InputFieldType;
  onKeyPress?: KeyPressHandler<HTMLInputElement>;
  onChange?: ChangeHandler<HTMLInputElement>;
};

export const InputField = (props: InputFieldProps) => {
  const {
    label,
    disabled,
    required,
    value,
    placeholder,
    className,
    fieldType = 'text',
    onChange,
    onKeyPress,
  } = props;
  const labelName = 'field-' + label.replace(' ', '').toLowerCase();
  return (
    <InputGroup className={className}>
      <InputGroup.Text id={labelName} className="label">
        {label}
      </InputGroup.Text>
      <FormControl
        value={value}
        onKeyUp={onKeyPress}
        onChange={onChange}
        as="input"
        disabled={disabled}
        required={required}
        type={fieldType}
        placeholder={placeholder}
        aria-label={label}
        aria-describedby={labelName}
      />
    </InputGroup>
  );
};
