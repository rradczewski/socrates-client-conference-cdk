import { Form, InputGroup } from 'react-bootstrap';
import { ChangeEventHandler, PropsWithChildren } from 'react';
import './InputField.scss';

type DropdownProps = {
  label: string;
  disabled?: boolean;
  required?: boolean;
  value: string;
  className?: string;
  onChange?: ChangeEventHandler<HTMLSelectElement>;
};

export const DropdownSelect = (props: PropsWithChildren<DropdownProps>) => {
  const { label, disabled, required, value, className, onChange } = props;
  const labelName = 'field-' + label.replace(' ', '').toLowerCase();
  return (
    <InputGroup className={className}>
      <InputGroup.Text id={labelName} className="label">
        {label}
      </InputGroup.Text>
      <Form.Control
        value={value}
        onChange={onChange}
        as="select"
        disabled={disabled}
        required={required}
        aria-label={label}
        aria-describedby={labelName}
      >
        {props.children}
      </Form.Control>
    </InputGroup>
  );
};
