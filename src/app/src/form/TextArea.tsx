import { Form, InputGroup } from 'react-bootstrap';
import React from 'react';
import { ChangeHandler } from './events';
import './InputField.scss';

type TextAreaProps = {
  label: string;
  value: string;
  placeholder: string;
  className?: string;
  rows?: number;
  onChange: ChangeHandler<HTMLTextAreaElement>;
};

export const TextArea = (props: TextAreaProps) => {
  const { label, value, placeholder, className, rows = 5, onChange } = props;
  const labelName = 'field-' + label.replace(' ', '').toLowerCase();
  return (
    <Form.Group className={className}>
      <InputGroup.Text id={labelName} className="label ">
        {label}
      </InputGroup.Text>
      <Form.Control
        value={value}
        onChange={onChange}
        as="textarea"
        rows={rows}
        placeholder={placeholder}
        aria-label={label}
        aria-describedby={labelName}
      />
    </Form.Group>
  );
};
