export enum MessageType {
  ALREADY_REGISTERED = 'ALREADY_REGISTERED',
  BAD_REQUEST = 'BAD_REQUEST',
  EMPTY = 'EMPTY',
  FAIL = 'FAIL',
  FOUNDATIONS = 'FOUNDATIONS',
  PROFILE_FAIL = 'PROFILE_FAIL',
  PROFILE_SUCCESS = 'PROFILE_SUCCESS',
  NOT_FOUND = 'NOT_FOUND',
  REGISTRATION_CLOSED = 'REGISTRATION_CLOSED',
  SUCCESS = 'SUCCESS',
  UNAUTHORIZED = 'UNAUTHORIZED',
  WITHDRAW_SUCCESS = 'WITHDRAW_SUCCESS',
}

export enum MessageKind {
  SUCCESS = 'success',
  WARN = 'warn',
  ERROR = 'error',
}

export const MESSAGES: Record<MessageType, string> = {
  ALREADY_REGISTERED:
    "We're sorry, a registration for this email already exists.\n\nThe good news is: You don't have to do anything - you're already in.\nUnless you can't access your data - in which case, please send an email to info@socrates-conference.de.",
  BAD_REQUEST:
    "We're sorry, unfortunately, we were unable to process your request.\n\nYou entered something we could not understand. Please try again.",
  EMPTY: '',
  FAIL: "We're sorry, unfortunately, the api seems to be unreachable.\n\nPlease try again later.",
  FOUNDATIONS:
    'Unfortunately, Training Day is currently sold out. Please note that your booking has been changed accordingly.',
  NOT_FOUND:
    "We're sorry, unfortunately, we couldn't identify the email you entered. Please check that your entry is correct.",
  PROFILE_FAIL:
    'Unfortunately, we could not process your request, because the backend could not be reached. Please try again later.',
  PROFILE_SUCCESS: 'Your profile was successfully updated.',
  REGISTRATION_CLOSED:
    "We're sorry, the registration is currently closed.\n\nSign up to our newsletter to stay informed, or follow us on Twitter @SoCraTes_Conf!",
  SUCCESS:
    "Thank you for caring about good software.\n\nYou have taken the first step towards being part of SoCraTes. Don't forget to log in and fill out your profile!",
  UNAUTHORIZED: "You don't have permission to access this profile.",
  WITHDRAW_SUCCESS:
    "You have successfully withdrawn your participation in SoCraTes.\n\nWe're sad to see you go. Please consider applying again next year!",
};

export const MESSAGE_TITLES: Record<MessageType, string> = {
  ALREADY_REGISTERED: 'Registration failed.',
  BAD_REQUEST: 'Bad Request.',
  EMPTY: '',
  FAIL: 'Temporary Problem.',
  FOUNDATIONS: 'Sorry, we have bad news:',
  NOT_FOUND: 'Registration failed.',
  PROFILE_FAIL: 'Request failed.',
  PROFILE_SUCCESS: 'Profile updated.',
  REGISTRATION_CLOSED: 'Registration closed.',
  SUCCESS: "You're registered!",
  UNAUTHORIZED: 'Access denied!',
  WITHDRAW_SUCCESS: 'Participation cancelled.',
};

export const MESSAGE_KINDS: Record<MessageType, MessageKind> = {
  ALREADY_REGISTERED: MessageKind.ERROR,
  BAD_REQUEST: MessageKind.ERROR,
  EMPTY: MessageKind.ERROR,
  FAIL: MessageKind.ERROR,
  FOUNDATIONS: MessageKind.WARN,
  NOT_FOUND: MessageKind.ERROR,
  PROFILE_FAIL: MessageKind.ERROR,
  PROFILE_SUCCESS: MessageKind.SUCCESS,
  REGISTRATION_CLOSED: MessageKind.ERROR,
  SUCCESS: MessageKind.SUCCESS,
  UNAUTHORIZED: MessageKind.ERROR,
  WITHDRAW_SUCCESS: MessageKind.SUCCESS,
};
