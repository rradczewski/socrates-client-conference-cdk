import { MessageKind } from './messages';
import React from 'react';

type MessageProps = {
  messageKind: MessageKind;
  messageTitle: string;
  messageHtml: string;
};

export function Message(props: MessageProps) {
  let { messageKind, messageTitle, messageHtml } = props;
  const msgClass =
    messageKind === MessageKind.SUCCESS
      ? 'alert-success'
      : messageKind === MessageKind.WARN
        ? 'alert-warning'
        : 'alert-danger';
  return (
    <div className={'row'}>
      <div className={'col-md-3'}></div>
      <div className={'col-md-6'}>
        <div
          id="application-message"
          className={'alert m-5 p-5 ' + msgClass}
          role="alert"
        >
          <h3>{messageTitle}</h3>
          <p
            className={'mt-4'}
            dangerouslySetInnerHTML={{
              __html: messageHtml.replaceAll('\n', '<br />'),
            }}
          ></p>
        </div>
      </div>
      <div className={'col-md-3'}></div>
    </div>
  );
}
