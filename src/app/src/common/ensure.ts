export function ensure<T>(
  input: T | undefined | null,
  message = 'Object must not be null',
): T {
  if (input === null || input === undefined) throw new Error(message);
  return input;
}
