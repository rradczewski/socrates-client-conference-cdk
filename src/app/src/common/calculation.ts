import {
  Booking,
  Conference,
  FlatFee,
  Package,
  RoomType,
  Stats,
} from '../main/types';
import moment from 'moment/moment';

export interface TicketOrder {
  flatFeeItems: FlatFee[];
  roomType: RoomType;
  arrival?: string;
  departure?: string;
  days?: number;
}

export interface Position {
  label: string;
  price: Price;
}

export interface Ticket {
  packages: Position[];
  accommodation: Position;
  total: Price;
  totalWithSponsoring: Price;
}

type Price = string;

export const parsePrice = (price: Price): number => {
  return parseFloat(price.replace(',', '.'));
};

export const formatEuro = (amount: number): Price => {
  let total: string = amount.toFixed(2);
  //if (total.endsWith('.00')) total = total.substring(0, total.length - 3)
  total = total.replace('.', ',');
  return total + '€';
};

export const calculateSponsorableAmount = (order: TicketOrder) => {
  const reducer = (total: number, f: FlatFee) =>
    f.excludeFromSponsoring ? total : total + parsePrice(f.price);
  const amount: any = order.flatFeeItems.reduce(reducer, 0);
  return formatEuro(amount);
};

function deriveNights(order: TicketOrder): number {
  if (order.arrival !== undefined && order.departure !== undefined) {
    return moment(order.departure).diff(moment(order.arrival), 'days');
  } else {
    return order.days ?? 0;
  }
}

export const calculateTicketPrice = (order: TicketOrder): Price => {
  const nights = deriveNights(order);
  const accommodation = nights * parsePrice(order.roomType.pricePerNight);
  const items = order.flatFeeItems.reduce(
    (total, item) => total + parsePrice(item.price),
    0,
  );
  const amount: number = accommodation + items;
  return formatEuro(amount);
};

const formatPlural = (term: string, amount: number) =>
  `${amount} ${amount === 1 ? term : term + 's'}`;
const parens = (str: string) => `(${str})`;

const toPosition = (item: RoomType | FlatFee, nights?: number): Position => {
  if (nights !== undefined) {
    return {
      label: `${item.description} ${parens(formatPlural('night', nights))}`,
      price: formatEuro(parsePrice(item.pricePerNight) * nights),
    };
  } else {
    return {
      label: item.description,
      price: formatEuro(parsePrice(item.price)),
    };
  }
};

function applySponsoring(
  totalPrice: Price,
  sponsorablePrice: Price,
  shareOfGroupFeesPrice: Price,
): Price {
  const total: number = parsePrice(totalPrice);
  const sponsorable: number = parsePrice(sponsorablePrice);
  const shareOfGroupFees: number = parsePrice(shareOfGroupFeesPrice);
  const totalWithSponsoring: number = total - sponsorable + shareOfGroupFees;
  return formatEuro(totalWithSponsoring);
}

export const calculateTicket = (
  order: TicketOrder,
  shareOfGroupFees: Price = '0€',
): Ticket => {
  const packages = order.flatFeeItems.map((i) => toPosition(i));
  const nights: number = deriveNights(order);
  const accommodation = toPosition(order.roomType, nights);
  const total: string = calculateTicketPrice(order);
  const sponsorable: string = calculateSponsorableAmount(order);
  const totalWithSponsoring = applySponsoring(
    total,
    sponsorable,
    shareOfGroupFees,
  );
  return {
    packages,
    accommodation,
    total,
    totalWithSponsoring,
  };
};

export function calculateIndividualShareOfGroupFees(
  stats: Stats,
  conference: Conference,
  booking: Booking,
): string {
  const sponsoringAvailable =
    parsePrice(stats.sponsoringAmount) - parsePrice(stats.expensesAmount);
  const sponsoringNeeded = Object.entries(stats.bookedPackages).reduce(
    (total, [pkgName, amount]) => {
      const pkg = conference?.flatFees?.find((f) => f.type === pkgName);
      const factor: number =
        amount + (booking.packages.indexOf(pkgName as Package) > -1 ? 1 : 0);
      const fee: number = pkg
        ? factor * (pkg.excludeFromSponsoring ? 0 : parsePrice(pkg.price))
        : 0;
      return total + fee;
    },
    0,
  );
  const groupFees = sponsoringNeeded - sponsoringAvailable;
  const attendees = stats.attendeeCount + 1; // include current user
  return formatEuro(groupFees > 0 ? groupFees / attendees : 0);
}
