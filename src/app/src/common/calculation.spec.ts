import {
  calculateIndividualShareOfGroupFees,
  calculateSponsorableAmount,
  calculateTicket,
  calculateTicketPrice,
  Ticket,
} from './calculation';
import {
  Conference,
  ConferenceState,
  FlatFee,
  RoomType,
  Stats,
} from '../main/types';

const SINGLE: RoomType = {
  type: 'single',
  description: 'Single Room',
  pricePerNight: '64€',
  beds: 1,
  count: 1,
};

const TRAINING_DAY: FlatFee = {
  type: 'training',
  description: 'Training Day',
  price: '100€',
  excludeFromSponsoring: true,
};
const SOCRATES: FlatFee = {
  type: 'conference',
  description: 'SoCraTes',
  price: '176,30€',
};

const JAN_1: string = '2022-01-01';
const JAN_2: string = '2022-01-02';
const JAN_3: string = '2022-01-03';

const STATS: Stats = {
  attendeeCount: 201,
  sponsoringAmount: '40000€',
  expensesAmount: '10000€',
  bookedPackages: { conference: 201, foundations: 23, workshops: 65 },
};

const CONF: Conference = {
  flatFees: [
    {
      description: 'SoCrates Unconference',
      excludeFromSponsoring: false,
      type: 'conference',
      price: '172,60€',
    },
    {
      description: 'Training Day',
      excludeFromSponsoring: true,
      type: 'training',
      price: '100,00€',
    },
    {
      description: 'Workshops Day',
      excludeFromSponsoring: false,
      type: 'workshops',
      price: '63,30€',
    },
  ],
  location: 'Hotel Park, Soltau',
  endDate: '2022-08-28',
  roomTypes: [
    {
      count: 120,
      description: 'Single Room',
      beds: 1,
      type: 'single',
      pricePerNight: '64,00€',
    },
    {
      count: 41,
      description: 'Junior Double Room (Single occupant)',
      beds: 1,
      type: 'junior-double',
      pricePerNight: '64,00€',
    },
    {
      count: 16,
      description: 'Bed in a shared Junior Double Room',
      beds: 2,
      type: 'junior-double-shared',
      pricePerNight: '47,95€',
    },
    {
      count: 14,
      description: 'Bed in a shared Double Room',
      beds: 2,
      type: 'double-shared',
      pricePerNight: '45,45€',
    },
  ],
  state: ConferenceState.REGISTRATION,
  byline: '11th International Conference for Software Craft and Testing',
  diversityRatio: 40,
  startDate: '2022-08-25',
  year: 2022,
  id: 'socrates_22',
  maxSeatsPerSponsor: 5,
  title: 'SoCraTes',
};

describe('calculateTicketPrice:', () => {
  it('should return 0€ for an empty order', () => {
    expect(
      calculateTicketPrice({
        flatFeeItems: [],
        roomType: SINGLE,
        arrival: JAN_1,
        departure: JAN_1,
      }),
    ).toEqual('0,00€');
  });
  it('should return 64€ for one night in a single room', () => {
    expect(
      calculateTicketPrice({
        flatFeeItems: [],
        roomType: SINGLE,
        arrival: JAN_1,
        departure: JAN_2,
      }),
    ).toEqual('64,00€');
  });
  it('should return 128€ for two nights in a single room', () => {
    expect(
      calculateTicketPrice({
        flatFeeItems: [],
        roomType: SINGLE,
        arrival: JAN_1,
        departure: JAN_3,
      }),
    ).toEqual('128,00€');
  });
  it('should return 100€ for Foundations Day only', () => {
    expect(
      calculateTicketPrice({
        flatFeeItems: [TRAINING_DAY],
        roomType: SINGLE,
        arrival: JAN_1,
        departure: JAN_1,
      }),
    ).toEqual('100,00€');
  });
  it('should return 276,30€ for Foundations Day and SoCraTes', () => {
    expect(
      calculateTicketPrice({
        flatFeeItems: [TRAINING_DAY, SOCRATES],
        roomType: SINGLE,
        arrival: JAN_1,
        departure: JAN_1,
      }),
    ).toEqual('276,30€');
  });
});

describe('calculateTicket:', () => {
  it('should return 64€ for one night in a single room', () => {
    expect(
      calculateTicket({
        flatFeeItems: [],
        roomType: SINGLE,
        arrival: JAN_1,
        departure: JAN_2,
      }),
    ).toEqual({
      packages: [],
      accommodation: { label: 'Single Room (1 night)', price: '64,00€' },
      total: '64,00€',
      totalWithSponsoring: '64,00€',
    });
  });
  it('should return 128€ for two nights in a single room', () => {
    expect(
      calculateTicket({
        flatFeeItems: [],
        roomType: SINGLE,
        arrival: JAN_1,
        departure: JAN_3,
      }),
    ).toEqual({
      packages: [],
      accommodation: { label: 'Single Room (2 nights)', price: '128,00€' },
      total: '128,00€',
      totalWithSponsoring: '128,00€',
    });
  });
  it('should return 100€ for Training Day only', () => {
    expect(
      calculateTicket({
        flatFeeItems: [TRAINING_DAY],
        roomType: SINGLE,
        arrival: JAN_1,
        departure: JAN_1,
      }),
    ).toEqual({
      packages: [{ label: 'Training Day', price: '100,00€' }],
      accommodation: { label: 'Single Room (0 nights)', price: '0,00€' },
      total: '100,00€',
      totalWithSponsoring: '100,00€',
    });
  });
  describe('when sponsoring is applied', () => {
    let result: Ticket;
    beforeEach(() => {
      result = calculateTicket(
        {
          flatFeeItems: [TRAINING_DAY, SOCRATES],
          roomType: SINGLE,
          arrival: JAN_1,
          departure: JAN_1,
        },
        '50€',
      );
    });
    it('should return 276,30€ total for Foundations Day and SoCraTes', () => {
      expect(result.total).toEqual('276,30€');
    });
    it('should return 150€ total after sponsoring is applied (50€ of group fees shared)', () => {
      expect(result.totalWithSponsoring).toEqual('150,00€');
    });
  });
});

describe('calculateSponsorableAmount:', () => {
  it('should include only flatfee items that are sponsorable', () => {
    expect(
      calculateSponsorableAmount({
        flatFeeItems: [TRAINING_DAY, SOCRATES],
        roomType: SINGLE,
        arrival: JAN_1,
        departure: JAN_2,
      }),
    ).toEqual('176,30€');
  });
});

describe('calculateIndividualShareOfGroupFees:', () => {
  it('should return sum of booked conference and workshops packages, divided by number of attendees', () => {
    // @ts-ignore
    const booking: Booking = { packages: ['conference', 'workshops'] };
    // Calculation for this example:
    // Sum of sponsorable conference fees: (202 * 172,60€ + 66 * 63,30€) = 39043€
    // Amount left after sponsoring: 39043€ - 30000€ = 9043€
    // Shared amount per attendee: 9043€ / 202 = 44,77€
    expect(calculateIndividualShareOfGroupFees(STATS, CONF, booking)).toEqual(
      '44,77€',
    );
  });
});
