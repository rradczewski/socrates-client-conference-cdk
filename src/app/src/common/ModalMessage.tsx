import { MessageKind } from './messages';
import React from 'react';
import { Button, Modal } from 'react-bootstrap';

type MessageProps = {
  messageKind: MessageKind;
  messageTitle: string;
  messageHtml: string;
  dialogAction: Function;
};

export function ModalMessage(props: MessageProps) {
  let { messageKind, messageTitle, messageHtml, dialogAction } = props;
  const msgClass =
    messageKind === MessageKind.SUCCESS
      ? 'alert-success'
      : messageKind === MessageKind.WARN
        ? 'alert-warning'
        : 'alert-danger';
  return (
    <Modal className={'p-0 m-0'} show={messageKind !== undefined}>
      <div className={'m-0 alert ' + msgClass} role="alert">
        <div>
          <h3>{messageTitle}</h3>
          <p
            className={'mt-4'}
            dangerouslySetInnerHTML={{
              __html: messageHtml.replaceAll('\n', '<br />'),
            }}
          ></p>
        </div>
        <hr />
        <div className={'text-end'}>
          <Button
            type={'submit'}
            onClick={() => {
              dialogAction();
            }}
          >
            OK
          </Button>
        </div>
      </div>
    </Modal>
  );
}
