import isValidEmailFormat from './isValidEmailFormat';

export const validationClassName = (isValid: boolean) =>
  isValid ? 'is-valid' : 'is-invalid';
export const checkboxValidationClassName = (isValid: boolean) =>
  `form-control ${isValid ? 'is-valid' : 'is-invalid'}`;
export const isValidNickname = (nickname: string) => nickname.trim().length > 0;
export const isValidEmail = (email?: string) =>
  email !== undefined && email.trim().length > 0 && isValidEmailFormat(email);
