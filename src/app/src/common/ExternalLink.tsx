import React from 'react';
type LinkProps = {
  url: string;
  target: string;
  title: string;
  children: any;
};
export default function ExternalLink({
  url,
  title,
  target = '_self',
  children,
}: LinkProps) {
  return (
    <a href={url} title={title} target={target}>
      {children}
    </a>
  );
}
