import { Nav, NavDropdown } from 'react-bootstrap';
import React, { useEffect, useState } from 'react';
import { Link, useLocation } from 'react-router-dom';
import { useConference } from '../main/ConferenceProvider';
import { Attendee, Conference } from '../main/types';
import { useProfile } from '../main/ProfileProvider';
import { useAuth } from '../main/AuthProvider';

export const Navigation = () => {
  const authContext = useAuth();
  const location: any = useLocation<any>();
  const conferenceContext = useConference();
  const profileContext = useProfile();
  const [conference, setConference] = useState<Conference>();
  const [attendee, setAttendee] = useState<Attendee>();

  useEffect(() => {
    if (!conference) {
      conferenceContext.conference().then(setConference);
    } else {
      profileContext.getProfile(conference.id).then((a) => {
        if (a) setAttendee(a.attendee);
      });
    }
  }, [conferenceContext, conference, profileContext]);

  const fullpath: string = location.pathname;
  const slashIndex: number = fullpath.indexOf('/', 1);
  const length: number = slashIndex > 0 ? slashIndex : fullpath.length;
  const mainpath: string = fullpath.substring(0, length);
  return (
    <header className="nav-header bg-primary shadow-sm">
      <div className="row align-items-center">
        <div className="col-12 col-md-4">
          <div className="d-flex align-items-center">
            <img
              className="logo"
              alt="SoCraTes logo"
              src="https://raw.githubusercontent.com/softwerkskammer/softwerkskammer-logos/master/SoCraTes/2024/SoCraTesWappen_2024.png"
            />
            <h5
              className={
                'conference-title text-light ml-2 ' +
                (conference ? '' : 'd-none')
              }
            >
              {conference?.title + ' ' + conference?.year} - Main Conference
            </h5>
          </div>
        </div>

        <div className="col-12 col-md-8">
          <Nav
            activeKey={mainpath}
            variant="pills"
            className="justify-content-end flex-md-row"
          >
            <Nav.Item>
              <Nav.Link as={Link} eventKey="/profile" to="/profile">
                My Profile
              </Nav.Link>
            </Nav.Item>
            {/*<Nav.Item>
              <Nav.Link
                as="a"
                className="nav-link"
                href="https://scrts.de/socrates2024/index.html"
              >
                Schedule
              </Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link
                as="a"
                className="nav-link"
                href="https://ping.socrates-conference.de"
              >
                Map
              </Nav.Link>
            </Nav.Item>*/}
            <NavDropdown
              id="nav-dropdown"
              title={attendee?.nickname}
              className=""
            >
              <NavDropdown.Item
                className="bg-primary nav-link"
                eventKey="signOut"
                onClick={() => authContext.signOut()}
              >
                Sign Out
              </NavDropdown.Item>
            </NavDropdown>
          </Nav>
        </div>
      </div>
    </header>
  );
};
