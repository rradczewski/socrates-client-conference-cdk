import React, { ChangeEventHandler, useEffect, useState } from 'react';
import './ConfirmationScreen.scss';
import { useConference } from '../main/ConferenceProvider';
import { Conference } from '../main/types';
import { Nav } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import ConfirmationForm from './ConfirmationForm';
import { isValidEmail } from '../common/validation';
import { Message } from '../common/Message';
import {
  MESSAGE_KINDS,
  MESSAGE_TITLES,
  MESSAGES,
  MessageType,
} from '../common/messages';
import { useAttendee } from '../main/AttendeeProvider';

type Props = {};

enum FormState {
  EDITING = 'EDITING',
  SUBMITTING = 'SUBMITTING',
  SUBMITTED = 'SUBMITTED',
}

const getConfirmationKey = () => {
  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);
  return urlParams.get('confirmationKey') ?? '';
};

export function ConfirmationScreen(): React.ReactElement<Props> {
  const conferenceContext = useConference();
  const attendeeContext = useAttendee();
  const [conference, setConference] = useState<Conference | undefined>();
  const [key, setKey] = useState(getConfirmationKey());
  const [email, setEmail] = useState('');
  const [dataPrivacyConfirmedChecked, setDataPrivacyConfirmedChecked] =
    useState(false);
  const [isDisabled, setDisabled] = useState(true);
  const [formState, setFormState] = useState(FormState.EDITING);
  const [messageType, setMessageType] = useState<MessageType | undefined>(
    undefined,
  );

  useEffect(() => {
    if (!conference) {
      conferenceContext.conference().then(setConference);
    } else {
      if (
        isValidEmail(email) &&
        (key ?? '').trim().length > 0 &&
        dataPrivacyConfirmedChecked
      )
        setDisabled(false);
      else setDisabled(true);
    }
  }, [
    conferenceContext,
    conference,
    email,
    key,
    dataPrivacyConfirmedChecked,
    formState,
  ]);

  const onEmailChange: ChangeEventHandler<HTMLInputElement> = (event) =>
    setEmail(event.currentTarget.value);
  const dataPrivacyConfirmedChange = () =>
    setDataPrivacyConfirmedChecked(!dataPrivacyConfirmedChecked);
  const onConsentKeyChange: ChangeEventHandler<HTMLInputElement> = (event) =>
    setKey(event.currentTarget.value);
  const onSubmit = async () => {
    setFormState(FormState.SUBMITTING);
    await attendeeContext
      .confirm(email, key)
      .then(() => setMessageType(MessageType.SUCCESS))
      .catch((e) => {
        console.error(JSON.stringify(e));
        if (e.type === 'AlreadyRegisteredException') {
          setMessageType(MessageType.ALREADY_REGISTERED);
        } else if (e.type === 'BadRequestException') {
          setMessageType(MessageType.BAD_REQUEST);
        } else if (e.type === 'NotFoundException') {
          setMessageType(MessageType.NOT_FOUND);
        } else {
          setMessageType(MessageType.FAIL);
        }
      });
    setFormState(FormState.SUBMITTED);
  };

  return (
    <div className={conference ? '' : 'd-none'}>
      <header className="nav-header bg-primary shadow-sm">
        <div className="row align-items-center">
          <div className="col">
            <div className="d-flex align-items-center">
              <img
                className="logo"
                alt="SoCraTes logo"
                src="https://raw.githubusercontent.com/softwerkskammer/softwerkskammer-logos/master/SoCraTes/2020/SoCraTesWappen_2020.png"
              />
              <h5
                className={
                  'conference-title text-light ml-2 ' +
                  (conference ? '' : 'd-none')
                }
              >
                {conference?.title + ' ' + conference?.year} - Main Conference
              </h5>
            </div>
          </div>
          <div className="col">
            <Nav variant="pills" className="justify-content-end">
              <Nav.Item>
                <Nav.Link
                  as={Link}
                  to={`/login`}
                  className="bg-primary text-light"
                >
                  Login
                </Nav.Link>
              </Nav.Item>
            </Nav>
          </div>
        </div>
      </header>

      <div className="row subnav shadow-sm justify-content-end p-1 pr-4">
        &nbsp;
      </div>
      {messageType !== undefined && (
        <Message
          messageKind={MESSAGE_KINDS[messageType]}
          messageTitle={MESSAGE_TITLES[messageType]}
          messageHtml={MESSAGES[messageType]}
        />
      )}
      <div className="confirmation p-0 mt-4 card">
        {formState !== FormState.SUBMITTED && (
          <ConfirmationForm
            email={email}
            hasValidEmail={isValidEmail(email)}
            onEmailChange={onEmailChange}
            consentKey={key}
            dataPrivacyConfirmedChange={dataPrivacyConfirmedChange}
            isDataPrivacyConfirmedChecked={dataPrivacyConfirmedChecked}
            onConsentKeyChange={onConsentKeyChange}
            onSubmit={onSubmit}
            isDisabled={isDisabled || formState === FormState.SUBMITTING}
          />
        )}
      </div>
    </div>
  );
}
