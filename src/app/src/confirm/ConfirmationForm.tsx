import React, { ChangeEventHandler, MouseEventHandler } from 'react';
import './ConfirmationForm.scss';
import ExternalLink from '../common/ExternalLink';
import { InputGroup } from 'react-bootstrap';
import { InputField } from '../form/InputField';

type Props = {
  email: string;
  hasValidEmail: boolean;
  onEmailChange: ChangeEventHandler<HTMLInputElement>;
  dataPrivacyConfirmedChange: () => void;
  isDataPrivacyConfirmedChecked: boolean;
  consentKey: string;
  onConsentKeyChange: ChangeEventHandler<HTMLInputElement>;
  isDisabled: boolean;
  onSubmit: () => void;
};

export default function ConfirmationForm(props: Props) {
  const klass = (isValid: boolean) =>
    `form-control p-0 mb-2 ${isValid ? 'is-valid' : 'is-invalid'}`;

  const checkklass = (isValid: boolean) =>
    `form-control p-0 ${isValid ? 'is-valid' : 'is-invalid'}`;

  const consentKeyClass = klass(props.consentKey !== '');
  const emailClass = klass(props.hasValidEmail);
  const dataPrivacyConfirmedClass = checkklass(
    props.isDataPrivacyConfirmedChecked,
  );

  const onSubmit: MouseEventHandler<HTMLButtonElement> = (event) => {
    event.preventDefault();
    props.onSubmit();
  };

  return (
    <div>
      <div className="card-header">
        <h3 className={'mt-2 mb-3'}>Welcome!</h3>
      </div>
      <form className="form px-3">
        <div className={'card-body'}>
          <div className="row">
            <div className="col-12">
              <p>
                Congratulations, you have won a seat at this year's SoCraTes
                conference!
              </p>
              <p>
                In order to set you up with an attendee account, we need you to
                confirm your participation by entering the e-mail address you
                signed up with, as well as the unique confirmation key that we
                sent to that same e-mail address.
              </p>
              <p>
                If you came here by clicking the confirmation link in our
                e-mail, these form fields should be pre-filled for you. But you
                can always enter the values manually.
              </p>
              <p>
                Once you have confirmed your participation, you will receive
                another e-mail with a one-time password. Please use it to log in
                and set a permanent password as soon as possible.
              </p>
              <p>
                After that, all that is left to do until you are a registered
                participant at SoCraTes, is to fill out your profile and submit
                all the information regarding your stay.
              </p>
              <p>We are looking forward to seeing you in Soltau!</p>
              <h5 className={'mb-1'}>The SoCraTes Organisers</h5>
            </div>
          </div>
          <hr />
          <div className="row">
            <div className="col-12 mb-3">
              <InputField
                label="Confirmation Key"
                className={`${consentKeyClass} fs-5`}
                placeholder="Your confirmation key"
                required
                onChange={props.onConsentKeyChange}
                value={props.consentKey}
              />
            </div>
          </div>
          <div className="row">
            <div className="col-12 mb-3">
              <InputField
                label="e-Mail"
                className={`${emailClass} fs-5`}
                placeholder="Your e-mail"
                required
                onChange={props.onEmailChange}
                value={props.email}
              />
            </div>
          </div>
          <div className="row">
            <div className="col-12 mb-3">
              <InputGroup>
                <InputGroup.Text>
                  <input
                    type="checkbox"
                    aria-label="Checkbox for following text input"
                    value="dataPrivacyConfirmed"
                    checked={props.isDataPrivacyConfirmedChecked}
                    onChange={props.dataPrivacyConfirmedChange}
                  />
                </InputGroup.Text>
                <label
                  className={`${dataPrivacyConfirmedClass} fs-5`}
                  style={{ height: '100%' }}
                >
                  <div className="p-2 pb-0">
                    <small>
                      <p>
                        I agree that the data provided during the registration
                        for SoCraTes (nickname and e-mail) will be collected and
                        processed for the purpose of creating an attendee
                        account.
                      </p>
                      <p>
                        Hint: You can revoke your consent at any time in the
                        future by canceling your participation via the profile
                        page.
                      </p>
                      <p>
                        Detailed information on handling user data can be found
                        in our{' '}
                        <ExternalLink
                          url={`https://socrates-conference.de/privacy-policy`}
                          target="_blank"
                          title="privacy policy"
                        >
                          {' '}
                          data privacy policy
                        </ExternalLink>
                      </p>
                    </small>{' '}
                  </div>
                </label>
              </InputGroup>
            </div>
          </div>
        </div>
        <div className="card-footer ">
          <div className={'text-end'}>
            <button
              id="confirmation-form-button"
              className="btn btn-primary mb-2 btn-lg"
              disabled={props.isDisabled}
              onClick={onSubmit}
            >
              Confirm participation
            </button>
          </div>
        </div>
      </form>
    </div>
  );
}
