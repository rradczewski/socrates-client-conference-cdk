import { InputGroup } from 'react-bootstrap';
import { hasPackage } from '../helpers';
import React from 'react';
import { Booking, Conference, Package, Stats } from '../../main/types';

type PackageBookingProps = {
  booking: Booking;
  conference: Conference;
  staff: boolean;
  stats: Stats;
  onPackageSelected: (pkg: Package) => void;
};
export const PackageBooking = ({
  booking,
  conference,
  stats,
  staff,
  onPackageSelected,
}: PackageBookingProps) => {
  return (
    <div>
      <div className={'row mt-3'}>
        <h6>Conference Packages</h6>
      </div>
      <div className={'row mt-3 justify-content-center'}>
        <div className={'col-10 col-md-8'}>
          {conference?.flatFees?.map((f, index) => (
            <div key={index}>
              <InputGroup className={'mb-2'}>
                <InputGroup.Text
                  id={`package${index}`}
                  className="p-1 ps-2 pe-2"
                >
                  <input
                    type="checkbox"
                    aria-label="Checkbox for following text input"
                    aria-describedby={`package-desc-${index} package-price-${index}`}
                    value={f.type}
                    disabled={
                      f.type === 'conference' ||
                      (f.type === 'training' && staff)
                    }
                    checked={
                      f.type === 'conference' || hasPackage(booking, f.type)
                    }
                    onChange={() => onPackageSelected(f.type)}
                  />
                </InputGroup.Text>
                <label
                  className="form-control p-0"
                  id={`package-desc-${index}`}
                >
                  <p className="ps-2 pb-2 pt-2 m-0">{f.description}</p>
                </label>
                <label
                  className="form-control p-0 price"
                  id={`package-price-${index}`}
                >
                  <p className="pe-2 pb-2 pt-2 m-0 text-end">{f.price}</p>
                </label>
              </InputGroup>
              {f.type === 'training' && hasPackage(booking, f.type) && (
                <p>
                  <small>
                    Please be aware that the amount of available seats for
                    Training Day is limited. Right now,{' '}
                    {stats.bookedPackages['training']} of 60 have already been
                    assigned. Until you have finished your profile submission,
                    we can not say for sure whether your own request will be
                    successful.
                  </small>
                </p>
              )}
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};
