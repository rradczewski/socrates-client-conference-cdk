import { Booking } from '../../main/types';
import React, { ChangeEvent } from 'react';
import { InputGroup } from 'react-bootstrap';
import { InputField } from '../../form/InputField';
import { DropdownSelect } from '../../form/DropdownSelect';

const tshirtModels = [
  {
    cut: 'straight',
    cutName: 'Straight',
    modelName: 'B&C #Organic E150 TU01B',
    modelUrl:
      'https://www.cantana.com/bio-und-fair/t-shirts/bc-organic-e150-tu01b/',
    sizeChartUrl:
      'https://www.cantana.com/wp-content/uploads/produktseiten-downloads/BC/ca_sizeguide/Cantana_TU01B_Groessentabelle.pdf',
    sizes: ['XS', 'S', 'M', 'L', 'XL', 'XXL', 'XXXL', '4XL', '5XL'],
  },
  {
    cut: 'curvy',
    cutName: 'Curvy',
    modelName: 'B&C #Organic E150 TW02B',
    modelUrl:
      'https://www.cantana.com/bio-und-fair/t-shirts/bc-organic-e150-tw02b/',
    sizeChartUrl:
      'https://www.cantana.com/wp-content/uploads/produktseiten-downloads/BC/ca_sizeguide/Cantana_TW02B_Groessentabelle.pdf',
    sizes: ['XS', 'S', 'M', 'L', 'XL', 'XXL', 'XXXL'],
  },
];

type ExtrasBookingProps = {
  booking: Booking;
  onChange: (
    field: string,
    event: ChangeEvent<HTMLInputElement | HTMLSelectElement>,
  ) => void;
  onCheckboxChange: (field: string) => void;
};
export const ExtrasBooking = ({
  booking,
  onCheckboxChange,
  onChange,
}: ExtrasBookingProps) => {
  return (
    <div>
      <div className="row mt-3">
        <h6>Extras</h6>
      </div>
      <div className="row mt-3 justify-content-center">
        <div className={'col-10 col-md-8'}>
          <InputGroup className="mb-2">
            <InputGroup.Text id={`swag-check`} className="p-1 ps-2 pe-2">
              <input
                type="checkbox"
                aria-label="Checkbox for following text input"
                value={'swag'}
                checked={Boolean(booking.swag)}
                onChange={() => onCheckboxChange('swag')}
              />
            </InputGroup.Text>
            <label className="form-control p-0">
              <p className="ps-2 pb-2 pt-2 m-0">
                I would like a SoCraTes t-shirt.
              </p>
            </label>
          </InputGroup>
          {Boolean(booking.swag) && (
            <DropdownSelect
              label={'T-Shirt Cut'}
              value={booking.swagCut ?? ''}
              onChange={(event) => onChange('swagCut', event)}
            >
              <option value="" disabled>
                Please select
              </option>
              {tshirtModels.map((model) => (
                <option value={model.cut} key={model.cut}>
                  {model.cutName}
                </option>
              ))}
            </DropdownSelect>
          )}
          {Boolean(booking.swag) && (
            <small>
              <p>
                Find more details about the models we plan to order on Cantana's
                website:
              </p>
              <ul>
                {tshirtModels.map((model) => (
                  <li key={model.cut}>
                    <a
                      target="_blank"
                      rel="noreferrer"
                      title={model.modelName}
                      href={model.modelUrl}
                    >
                      {model.cutName}
                    </a>{' '}
                    (
                    <a
                      target="_blank"
                      rel="noreferrer"
                      title={`Size Chart for ${model.cutName}`}
                      href={model.sizeChartUrl}
                    >
                      Size Chart
                    </a>
                    )
                  </li>
                ))}
              </ul>
            </small>
          )}
          {Boolean(booking.swag) && !!booking.swagCut && (
            <DropdownSelect
              label={'T-Shirt Size'}
              value={booking.swagSize ?? ''}
              onChange={(event) => onChange('swagSize', event)}
            >
              <option value="" disabled>
                Please select
              </option>
              {tshirtModels
                .find((model) => model.cut === booking.swagCut)
                ?.sizes?.map((size) => (
                  <option key={size} value={size}>
                    {size}
                  </option>
                ))}
            </DropdownSelect>
          )}
          {!Boolean(booking.swag) && (
            <p>
              <small>
                In order to reduce waste and keep our carbon footprint low, we
                have decided to make the give-aways optional. Please opt-in to
                receive a SoCraTes t-shirt, if you would like one.
              </small>
            </p>
          )}
          <InputField
            className={'mt-4'}
            label={'Dietary requirements'}
            value={booking.dietary ?? ''}
            onChange={(event) => onChange('dietary', event)}
            placeholder={'none'}
          />
          <p>
            <small>
              If you are vegetarian/vegan, have any allergies or intolerances,
              please tell us. While we cannot promise to have a different meal
              for everyone, we will try our best to make the menu work for you.
            </small>
          </p>
        </div>
      </div>
    </div>
  );
};
