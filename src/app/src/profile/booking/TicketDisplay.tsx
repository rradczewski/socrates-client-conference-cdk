import { Ticket } from '../../common/calculation';
import React from 'react';
import './TicketDisplay.scss';
import { config } from '../../config';

type TicketDisplayProps = { order?: Ticket; totalAttendees: number };

export function TicketDisplay({ order, totalAttendees }: TicketDisplayProps) {
  const calculateSponsoring =
    totalAttendees >= config.minimumSponsoringAttendees;
  return (
    <div className="row mt-3">
      <div className="col-12">
        <hr />
      </div>
      <div className="row mt-3 justify-content-center">
        <div className={'col-10 col-md-8 mt-4'}>
          {order && (
            <table className="ticket shadow">
              <thead>
                <tr>
                  <th colSpan={2}>
                    <h4>Your SoCraTes Ticket</h4>
                  </th>
                </tr>
              </thead>
              <tbody>
                {order.packages.map((p, index) => (
                  <tr key={index}>
                    <td>{p.label}</td>
                    <td className="text-end">{p.price}</td>
                  </tr>
                ))}

                <tr>
                  <td>{order.accommodation.label}</td>
                  <td className="text-end">{order.accommodation.price}</td>
                </tr>
                <tr className="total">
                  <td>Total (without sponsoring):</td>
                  <td className="text-end">{order.total}</td>
                </tr>
                <tr className="expected-total">
                  <td>
                    Expected total (with sponsoring)<sup>*</sup>:
                  </td>
                  <td className="text-end">
                    {calculateSponsoring ? order.totalWithSponsoring : '??? €'}
                  </td>
                </tr>
              </tbody>
            </table>
          )}
        </div>
      </div>
      <div className="row mt-3 justify-content-center">
        {calculateSponsoring ? (
          <div className="col-12 col-sm-8 col-md-6 footnote mt-4">
            <small>
              * Please bear in mind that due to people canceling or signing up,
              or changing their duration of stay, we cannot reliably calculate
              the final price until two days before the conference. The value
              shown here is a preliminary calculation, based on the current
              number of reservations and amount of sponsoring funds collected.
            </small>
          </div>
        ) : (
          <div className="col-12 col-sm-8 col-md-6 footnote mt-4">
            <small>
              * The expected total price will not be calculated until more than
              90% of available seats have been claimed and confirmed, to make
              sure the results are reasonably stable. Check back in a few days -
              this should not take long!
            </small>
          </div>
        )}
      </div>
    </div>
  );
}
