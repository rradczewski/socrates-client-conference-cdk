import { Booking, Conference, Day } from '../../main/types';
import { InputField } from '../../form/InputField';
import React from 'react';
import { hasDay, hasFoundations, hasWorkshops } from '../helpers';
import { InputGroup } from 'react-bootstrap';
import { ChangeEvent } from 'react';
import { TextArea } from '../../form/TextArea';

type DaySelectProps = {
  day: Day;
  disabled: boolean;
  booking: Booking;
  onDaySelected: (day: Day) => void;
};
const DaySelect = ({
  day,
  disabled,
  booking,
  onDaySelected,
}: DaySelectProps) => {
  return (
    <td>
      <input
        type="checkbox"
        value={day}
        disabled={disabled}
        checked={hasDay(booking, day)}
        onChange={() => onDaySelected(day)}
      />
    </td>
  );
};
export type ChildCareField =
  | 'childcareRequired'
  | 'children'
  | 'childcareDetails';

type RoomBookingProps = {
  booking: Booking;
  conference: Conference;
  staff: boolean;
  onCheckboxChange: (field: string) => void;
  onChildcareChange: (
    field: ChildCareField,
    event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
  ) => void;
  onDaySelected: (day: Day) => void;
};
export const RoomBooking = ({
  booking,
  conference,
  staff,
  onCheckboxChange,
  onChildcareChange,
  onDaySelected,
}: RoomBookingProps) => {
  return (
    <div>
      <div className="row mt-3">
        <h6>Room Booking</h6>
      </div>
      <div className="row mt-3 justify-content-center">
        <div className="col-12 col-md-8 booking">
          <InputField
            label={'Roomtype'}
            disabled
            value={
              conference?.roomTypes?.find(
                (r) => r.type === booking.room.roomType,
              )?.description ?? ''
            }
            placeholder={'none'}
          />

          <div>
            {booking.room.roommate && (
              <InputField
                className={'mt-2'}
                label={'Roommate'}
                disabled
                value={booking?.room.roommate ?? ''}
                placeholder={'none'}
              />
            )}
            <div>
              <table className="w-100 days">
                <thead>
                  <tr>
                    <th>Wed</th>
                    <th>Thu</th>
                    <th>Fri</th>
                    <th>Sat</th>
                    <th>Sun</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <DaySelect
                      day={'Wed'}
                      disabled={!hasFoundations(booking) && !staff}
                      booking={booking}
                      onDaySelected={onDaySelected}
                    />
                    <DaySelect
                      day={'Thu'}
                      disabled={true}
                      booking={booking}
                      onDaySelected={onDaySelected}
                    />
                    <DaySelect
                      day={'Fri'}
                      disabled={true}
                      booking={booking}
                      onDaySelected={onDaySelected}
                    />
                    <DaySelect
                      day={'Sat'}
                      disabled={hasWorkshops(booking)}
                      booking={booking}
                      onDaySelected={onDaySelected}
                    />
                    <DaySelect
                      day={'Sun'}
                      disabled={!hasWorkshops(booking)}
                      booking={booking}
                      onDaySelected={onDaySelected}
                    />
                  </tr>
                </tbody>
              </table>
            </div>
            <InputGroup className={'mt-2'}>
              <InputGroup.Text>
                <input
                  type="checkbox"
                  value={'family'}
                  checked={booking.room.family}
                  onChange={() => onCheckboxChange('family')}
                />
              </InputGroup.Text>
              <label className={'form-control'}>
                {booking?.room.roommate ? 'We' : 'I'} would like to bring a
                child/children.
              </label>
            </InputGroup>
            {booking.room.family && (
              <>
                <p className={'m-2'}>
                  <small>
                    We absolutely love that you want to bring your family! There
                    are a limited amount of rooms equipped for children at the
                    hotel. We will get in touch with you via email to discuss
                    the details.
                  </small>
                </p>
                <InputGroup className={'mt-2'}>
                  <InputGroup.Text>
                    <input
                      type="checkbox"
                      value={'family'}
                      checked={!!booking.childcare}
                      onChange={(e) =>
                        onChildcareChange('childcareRequired', e)
                      }
                    />
                  </InputGroup.Text>
                  <label className={'form-control'}>
                    {booking?.room.roommate ? 'We' : 'I'} require professional
                    childcare during the conference days.
                  </label>
                </InputGroup>
                {booking.childcare && (
                  <>
                    <p className={'mt-2'}>
                      <small>
                        Childcare will be provided by a professional provider at
                        no additional cost to you. Please note, that childcare
                        is limited to the main conference days (Friday and
                        Saturday). Childcare opening hours will cover the
                        marketplace, the morning sessions and the afternoon
                        sessions (8:45-12:30 and 13:45-17:30).
                      </small>
                    </p>
                    <div className="input-group mt-2">
                      <label className="input-group-text" id="basic-addon1">
                        How many children will require childcare?
                      </label>
                      <input
                        className={'form-control'}
                        type="number"
                        min="1"
                        placeholder="1"
                        value={booking.childcare?.children?.toString() ?? '1'}
                        onChange={(e) => onChildcareChange('children', e)}
                      />
                    </div>

                    <p className={'mt-2'}>
                      <small>
                        We would appreciate it, if you could share some details
                        with us, such as the age and which language your
                        child/children speaks. This will help us a lot in making
                        the proper arrangements. We will get in touch with you
                        via email regarding further clarifications.
                      </small>
                    </p>
                    <TextArea
                      className={'mt-2'}
                      label="Tell us more (e.g. age, spoken languages, special needs,...)"
                      rows={4}
                      value={booking.childcare?.details ?? ''}
                      placeholder=""
                      onChange={(e) => onChildcareChange('childcareDetails', e)}
                    />
                  </>
                )}
              </>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};
