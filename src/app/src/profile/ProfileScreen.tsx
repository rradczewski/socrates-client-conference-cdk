import React, {
  ChangeEvent,
  MouseEventHandler,
  useEffect,
  useState,
} from 'react';
import './ProfileScreen.scss';
import {
  Attendee,
  BillingAddress,
  Booking,
  ChildcareRequirements,
  Conference,
  Day,
  Package,
  Stats,
} from '../main/types';
import { useConference } from '../main/ConferenceProvider';
import { useProfile } from '../main/ProfileProvider';
import {
  MESSAGE_KINDS,
  MESSAGE_TITLES,
  MESSAGES,
  MessageType,
} from '../common/messages';
import { Navigation } from '../navigation/Navigation';
import { Button, Form, InputGroup } from 'react-bootstrap';
import {
  BsFillExclamationTriangleFill,
  BsFillPersonCheckFill,
  BsFillShieldFill,
} from 'react-icons/bs';
import {
  calculateIndividualShareOfGroupFees,
  calculateTicket,
  TicketOrder,
} from '../common/calculation';
import { ensure } from '../common/ensure';
import { TicketDisplay } from './booking/TicketDisplay';
import { BillingAddressForm } from './personal-data/BillingAddressForm';
import { AccountData } from './personal-data/AccountData';
import { useAuth } from '../main/AuthProvider';
import { useBooking } from '../main/BookingProvider';
import { PrivacyCheckbox } from './personal-data/PrivacyCheckbox';
import { PackageBooking } from './booking/PackageBooking';
import { ChildCareField, RoomBooking } from './booking/RoomBooking';
import { ExtrasBooking } from './booking/ExtrasBooking';
import { BadgeData } from './personal-data/BadgeData';
import { ModalMessage } from '../common/ModalMessage';
import { hasDay, hasPackage } from './helpers';
import { PersonalAddressForm } from './personal-data/PersonalAddressForm';

const removeDay = (book: Booking, day: Day): void => {
  const index: number = book.room.daysSelected?.indexOf(day) ?? -1;
  if (index > -1) {
    book.room.daysSelected?.splice(index, 1);
  }
};

const addDay = (book: Booking, day: Day): void => {
  if (book.room.daysSelected?.indexOf(day) === -1) {
    book.room.daysSelected.push(day);
  }
};

function adjustOvernightStays(booking: Booking, packages: Package[]): Booking {
  const book: Booking = { ...booking, packages };

  if (packages.some((f) => f === 'workshops')) {
    // Saturday night is mandatory if attending workshops
    addDay(book, 'Sat');
  } else {
    // Sunday night is not selectable if not attending workshops
    removeDay(book, 'Sun');
  }
  if (!packages.some((f) => f === 'training')) {
    // Wednesday night is not selectable if not attending foundations
    removeDay(book, 'Wed');
  }
  return book;
}

const addOrRemoveDay = (book: Day[], day: Day): void => {
  const index: number = book.indexOf(day);
  if (index > -1) book.splice(index, 1);
  else book.push(day);
};

function addOrRemovePackage(
  packages: Package[],
  pkg: 'conference' | 'training' | 'workshops',
): void {
  const index: number = packages.indexOf(pkg);
  if (index > -1) {
    packages.splice(index, 1);
  } else {
    packages.push(pkg);
  }
}

export function ProfileScreen() {
  const authContext = useAuth();
  const conferenceContext = useConference();
  const shoppingContext = useBooking();
  const profileContext = useProfile();
  const [conference, setConference] = useState<Conference>();
  const [stats, setStats] = useState<Stats>();
  const [attendee, setAttendee] = useState<Attendee>();
  const [booking, setBooking] = useState<Booking>();
  const [messageType, setMessageType] = useState<MessageType | undefined>(
    undefined,
  );
  const [dialogAction, setDialogAction] = useState('close');

  useEffect(() => {
    if (!conference) {
      conferenceContext.conference().then(setConference);
    } else {
      if (!stats) {
        shoppingContext
          .getStats(conference.id)
          .then((s) => setStats(s))
          .catch((e) => {
            console.error(e);
          });
      }
      if (!attendee) {
        profileContext
          .getProfile(conference.id)
          .then((a) => {
            setAttendee(a?.attendee);
            const book: Booking | undefined = a?.booking;
            if (book) {
              addDay(book, 'Fri');
              if (
                a?.attendee.reason !== 'staff' &&
                hasDay(book, 'Wed') &&
                !hasPackage(book, 'training')
              ) {
                removeDay(book, 'Wed');
              }
            }
            setBooking(book);
          })
          .catch((e) => {
            console.error(e);
            setMessageType(MessageType.UNAUTHORIZED);
          });
      }
    }
  }, [
    conferenceContext,
    conference,
    profileContext,
    attendee,
    booking,
    shoppingContext,
    stats,
    setMessageType,
  ]);

  const onDaySelected = (day: Day) => {
    if (booking) {
      const daysSelected = booking.room.daysSelected;
      addOrRemoveDay(daysSelected, day);
      const book: Booking = {
        ...booking,
        room: { ...booking.room, daysSelected },
      };
      setBooking(book);
    }
  };

  const onPackageSelected = (pkg: Package) => {
    if (booking) {
      const packages = booking.packages;
      addOrRemovePackage(packages, pkg);

      const book = adjustOvernightStays(booking, packages);

      setBooking(book);
    }
  };

  const processOrder = () => {
    if (conference && booking && stats) {
      const order: TicketOrder = {
        flatFeeItems: [
          ...ensure(
            conference.flatFees?.filter(
              (f) => booking.packages.indexOf(f.type) !== -1,
            ),
            'At least one conference package should have been booked.',
          ),
        ],
        roomType: ensure(
          conference.roomTypes?.find((r) => r.type === booking.room.roomType),
          booking.room.roomType + ' did not match an available room type.',
        ),
        days: booking.room.daysSelected.length,
      };
      return calculateTicket(
        order,
        calculateIndividualShareOfGroupFees(stats, conference, booking),
      );
    }
  };

  const onChangePersonalAddress = (
    field: string,
    event: ChangeEvent<HTMLInputElement>,
  ) => {
    if (attendee) {
      const address = attendee.personalAddress ?? {};
      const updatedAddress = { ...address } as Record<string, any>;
      updatedAddress[field] = event.currentTarget.value;
      const updatedBillingAddress = attendee.billingAddressIsPersonalAddress
        ? updatedAddress
        : attendee.billingAddress;
      setAttendee({
        ...attendee,
        personalAddress: updatedAddress,
        billingAddress: updatedBillingAddress,
      } as Attendee);
    }
  };

  const onChangeBillingAddress = (
    field: string,
    event: ChangeEvent<HTMLInputElement>,
  ) => {
    if (attendee) {
      const address = attendee.billingAddress ?? {};
      const updatedAddress = { ...address } as Record<string, any>;
      updatedAddress[field] = event.currentTarget.value;
      setAttendee({ ...attendee, billingAddress: updatedAddress } as Attendee);
    }
  };

  const onChangeAttendee = (
    field: string,
    event: ChangeEvent<HTMLInputElement>,
  ) => {
    if (attendee) {
      const att = { ...attendee } as Record<string, any>;
      att[field] = event.currentTarget.value;
      setAttendee(att as Attendee);
    }
  };
  const onChangeBooking = (
    field: string,
    event: ChangeEvent<HTMLInputElement | HTMLSelectElement>,
  ) => {
    if (booking) {
      const book = { ...booking } as Record<string, any>;
      book[field] = event.currentTarget.value;
      setBooking(book as Booking);
    }
  };

  const onChangeChildcare = (
    field: ChildCareField,
    event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
  ) => {
    if (!booking) {
      return;
    }

    if (field === 'childcareRequired') {
      if (booking.childcare) {
        setBooking({ ...booking, childcare: undefined });
      } else {
        const childcareRequirements: ChildcareRequirements = {
          children: 1,
          details: '',
        };
        const bookingWithChildcare = {
          ...booking,
          childcare: childcareRequirements,
        };
        setBooking(bookingWithChildcare);
      }
      return;
    }

    if (field === 'children' && booking.childcare) {
      const updateChildcare = {
        ...booking.childcare,
        children: parseInt(event.currentTarget.value, 10),
      };
      setBooking({ ...booking, childcare: updateChildcare });
    }
    if (field === 'childcareDetails' && booking.childcare) {
      const updateChildcare = {
        ...booking.childcare,
        details: event.currentTarget.value,
      };
      setBooking({ ...booking, childcare: updateChildcare });
    }
  };

  const onChangeBillingIsPersonalAddress = () => {
    const current = attendee?.billingAddressIsPersonalAddress ?? false;
    onCheckboxChangeAttendee('billingAddressIsPersonalAddress');
    const updatedAttendee = {
      ...attendee,
      billingAddressIsPersonalAddress: !current,
      billingAddress: current ? undefined : attendee?.personalAddress,
    } as Attendee;
    setAttendee(updatedAttendee);
  };

  const onCheckboxChangeAttendee = (field: string) => {
    if (attendee) {
      const att = { ...attendee } as Record<string, any>;
      att[field] = !att[field];
      setAttendee(att as Attendee);
    }
  };
  const onCheckboxChangeBooking = (field: string) => {
    if (booking) {
      const book = { ...booking } as Record<string, any>;
      book[field] = !book[field];
      setBooking(book as Booking);
    }
  };
  const onCheckboxChangeRoom = (field: string) => {
    if (booking) {
      const book = { ...booking, room: { ...booking.room } } as Record<
        string,
        any
      >;
      book.room[field] = !book.room[field];
      setBooking(book as Booking);
    }
  };

  const checkboxClass = (isValid: boolean) =>
    `form-control p-0 ${isValid ? 'is-valid' : 'is-invalid'}`;

  const isBillingAddressComplete = (address: BillingAddress | undefined) => {
    const notEmpty = (input: string | undefined) => {
      return input && input.trim().length > 0;
    };
    return (
      address &&
      notEmpty(address.firstname) &&
      notEmpty(address.lastname) &&
      notEmpty(address.address1) &&
      notEmpty(address.city) &&
      notEmpty(address.postal) &&
      notEmpty(address.country)
    );
  };

  const onCancel = async () => {
    if (conference)
      await profileContext
        .cancelParticipation(conference?.id)
        .catch((e) => {
          if (e.type === 'UnauthorizedException') {
            setMessageType(MessageType.UNAUTHORIZED);
          }
          if (e.type === 'NotFoundException')
            setMessageType(MessageType.NOT_FOUND);
          if (e.type === 'NetworkException')
            setMessageType(MessageType.PROFILE_FAIL);
        })
        .then(async () => {
          setDialogAction('cancel');
          setMessageType(MessageType.WITHDRAW_SUCCESS);
        });
  };

  const onCancelClose = async () => {
    setMessageType(undefined);
    await authContext.signOut();
    window.location.href = 'https://socrates-conference.de';
  };

  const onClose = async () => {
    setMessageType(undefined);
    if (conference) {
      const profile = await profileContext.getProfile(conference?.id);
      if (profile) {
        setAttendee(profile.attendee);
        setBooking(profile.booking);
      }
    }
  };

  const onSubmit: MouseEventHandler<HTMLButtonElement> = async (event) => {
    event.preventDefault();
    setDialogAction('close');
    await profileContext
      .updateProfile(
        ensure(
          attendee,
          'Something went wrong: The submitted attendee profile is undefined.',
        ),
        ensure(
          booking,
          'Something went wrong: The submitted booking is undefined.',
        ),
      )
      .catch((e) => {
        console.error('Outer:', e);
        if (e.type === 'UnauthorizedException')
          setMessageType(MessageType.UNAUTHORIZED);
        if (e.type === 'NotFoundException')
          setMessageType(MessageType.NOT_FOUND);
        if (e.type === 'NetworkException')
          setMessageType(MessageType.PROFILE_FAIL);
      })
      .then(async (message) => {
        if ((message ?? '').indexOf('Training') > -1) {
          setMessageType(MessageType.FOUNDATIONS);
        } else if (message === undefined) {
          setMessageType(MessageType.PROFILE_SUCCESS);
        }
      });
  };

  return (
    <div>
      <Navigation />
      {attendee && (
        <div className="row mt-2 mb-2">
          <div className="profile card pb-4 col-12 col-sm-10 col-md-8 mx-auto">
            <Form>
              <div className={'card-header mt-2'}>
                <div className={'row'}>
                  <div className={'col-6 pt-1'}>
                    <h3 className="profile-nickname">
                      <span>
                        {attendee?.reason === 'staff' ? (
                          <BsFillShieldFill className={'mb-2 text-success '} />
                        ) : attendee.reason === 'sponsor' ? (
                          <BsFillPersonCheckFill
                            className={'mb-2 text-success'}
                          />
                        ) : (
                          <></>
                        )}
                      </span>
                      {attendee?.nickname}'s Profile
                    </h3>
                  </div>
                  <div className={'col-6'}>
                    <Button
                      variant="danger"
                      onClick={onCancel}
                      className="small float-end m-0 cancel-button"
                    >
                      <BsFillExclamationTriangleFill
                        className={'mb-1 me-2 d-none d-lg-inline-block'}
                      />
                      Cancel Participation
                      <BsFillExclamationTriangleFill
                        className={'mb-1 ms-2 d-none d-lg-inline-block'}
                      />
                    </Button>
                  </div>
                </div>
              </div>
              <div className={'card-body'}>
                <AccountData
                  attendee={ensure(attendee)}
                  onChange={onChangeAttendee}
                />
                <hr />
                <PersonalAddressForm
                  address={ensure(attendee).personalAddress ?? {}}
                  onChange={onChangePersonalAddress}
                />
                <hr />
                <InputGroup className={'mt-2'}>
                  <InputGroup.Text>
                    <input
                      type="checkbox"
                      value={'billingAddressIsPersonalAddress'}
                      checked={attendee.billingAddressIsPersonalAddress}
                      onChange={onChangeBillingIsPersonalAddress}
                    />
                  </InputGroup.Text>
                  <label className={'form-control'}>
                    Billing address is personal address.
                  </label>
                </InputGroup>
                {!attendee.billingAddressIsPersonalAddress && (
                  <BillingAddressForm
                    address={ensure(attendee).billingAddress ?? {}}
                    onChange={onChangeBillingAddress}
                  />
                )}
                <hr />
                <BadgeData
                  attendee={ensure(attendee)}
                  onChange={onChangeAttendee}
                />
                <hr />
                <PrivacyCheckbox
                  checked={attendee.privacy ?? false}
                  className={checkboxClass(attendee.privacy)}
                  onCheckboxChange={onCheckboxChangeAttendee}
                />
                <hr />
                {isBillingAddressComplete(attendee.billingAddress) &&
                  attendee.privacy &&
                  booking &&
                  stats && (
                    <div>
                      <PackageBooking
                        booking={booking}
                        conference={ensure(conference)}
                        stats={ensure(stats)}
                        staff={attendee.reason === 'staff'}
                        onPackageSelected={onPackageSelected}
                      />
                      <hr />
                      <RoomBooking
                        booking={ensure(booking)}
                        conference={ensure(conference)}
                        staff={attendee.reason === 'staff'}
                        onCheckboxChange={onCheckboxChangeRoom}
                        onDaySelected={onDaySelected}
                        onChildcareChange={onChangeChildcare}
                      />
                      <hr />
                      <ExtrasBooking
                        booking={booking}
                        onChange={onChangeBooking}
                        onCheckboxChange={onCheckboxChangeBooking}
                      />
                      <TicketDisplay
                        order={processOrder()}
                        totalAttendees={ensure(stats).attendeeCount}
                      />
                    </div>
                  )}
              </div>
              <div className={'card-footer'}>
                <Button variant={'primary'} type={'submit'} onClick={onSubmit}>
                  Submit Profile
                </Button>
              </div>
            </Form>
          </div>
        </div>
      )}

      {messageType !== undefined && (
        <ModalMessage
          dialogAction={dialogAction === 'cancel' ? onCancelClose : onClose}
          messageKind={MESSAGE_KINDS[messageType]}
          messageTitle={MESSAGE_TITLES[messageType]}
          messageHtml={MESSAGES[messageType]}
        />
      )}
    </div>
  );
}
