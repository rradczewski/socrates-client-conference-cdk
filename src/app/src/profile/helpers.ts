import { Booking, Package } from '../main/types';

export const hasPackage = (booking: Booking | undefined, pkg: Package) => {
  return booking?.packages.some((f) => f === pkg) ?? false;
};

export const hasFoundations = (booking: Booking | undefined) => {
  return hasPackage(booking, 'training');
};

export const hasWorkshops = (booking: Booking | undefined) => {
  return hasPackage(booking, 'workshops');
};

export const hasDay = (booking: Booking | undefined, day: string) => {
  return booking?.room.daysSelected?.some((f) => f === day) ?? false;
};
