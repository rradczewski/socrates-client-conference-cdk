import { BillingAddress } from '../../main/types';
import React, { ChangeEvent } from 'react';
import { InputField } from '../../form/InputField';

type BillingAddressProps = {
  address: BillingAddress;
  onChange: (field: string, event: ChangeEvent<HTMLInputElement>) => void;
};
export const BillingAddressForm = ({
  address,
  onChange,
}: BillingAddressProps) => {
  const inputClass = (isValid: boolean) =>
    `form-control p-0 mb-2 ${isValid ? 'is-valid' : 'is-invalid'}`;
  return (
    <div>
      <div className="row mt-3">
        <h6>Billing Address</h6>
      </div>
      <div className="row mt-1">
        <p>
          <small>
            This address is used to create the invoices.
            <br />
            Please specify the person or company who shall pay for your ticket.
          </small>
        </p>
      </div>
      <div className="row mt-3">
        <div className="col-12 col-md-6">
          <InputField
            className={inputClass((address.firstname?.trim() ?? '') !== '')}
            label="First name"
            value={address?.firstname ?? ''}
            placeholder="Payer first name"
            required
            onChange={(event) => onChange('firstname', event)}
          />
        </div>
        <div className="col-12 col-md-6">
          <InputField
            className={inputClass((address.lastname?.trim() ?? '') !== '')}
            label="Last name"
            value={address?.lastname ?? ''}
            placeholder="Payer last name"
            required
            onChange={(event) => onChange('lastname', event)}
          />
        </div>
      </div>
      <div className="row mt-3">
        <div className="col-12">
          <InputField
            className="mt-0 mb-2"
            label="Company"
            value={address?.company ?? ''}
            placeholder="Payer company name"
            onChange={(event) => onChange('company', event)}
          />
        </div>
        <div className="col-12">
          <InputField
            className="mt-0 mb-2"
            label="VAT Number"
            value={address?.vat ?? ''}
            placeholder="Payer VAT number"
            onChange={(event) => onChange('vat', event)}
          />
        </div>
      </div>
      <div className="row mt-3">
        <div className="col-12">
          <InputField
            className={inputClass((address.address1?.trim() ?? '') !== '')}
            label="Address 1"
            value={address?.address1 ?? ''}
            placeholder="Address line 1"
            required={true}
            onChange={(event) => onChange('address1', event)}
          />
        </div>
      </div>
      <div className="row mt-3">
        <div className="col-12">
          <InputField
            className="mt-0 mb-2"
            label="Address 2"
            value={address?.address2 ?? ''}
            placeholder="Address line 2"
            onChange={(event) => onChange('address2', event)}
          />
        </div>
      </div>
      <div className="row mt-3">
        <div className="col-12 col-md-6">
          <InputField
            className={inputClass((address.postal?.trim() ?? '') !== '')}
            label="Postal code"
            value={address?.postal ?? ''}
            placeholder="Payer postal code"
            required={true}
            onChange={(event) => onChange('postal', event)}
          />
        </div>
        <div className="col-12 col-md-6">
          <InputField
            className={inputClass((address.city?.trim() ?? '') !== '')}
            label="City"
            value={address?.city ?? ''}
            placeholder="Payer city"
            required={true}
            onChange={(event) => onChange('city', event)}
          />
        </div>
      </div>
      <div className="row mt-3">
        <div className="col-12">
          <InputField
            className={inputClass((address.country?.trim() ?? '') !== '')}
            label="Country"
            value={address?.country ?? ''}
            placeholder="Country"
            required={true}
            onChange={(event) => onChange('country', event)}
          />
        </div>
      </div>
    </div>
  );
};
