import { Address } from '../../main/types';
import React, { ChangeEvent } from 'react';
import { InputField } from '../../form/InputField';

type PersonalAddressProps = {
  address: Address;
  onChange: (field: string, event: ChangeEvent<HTMLInputElement>) => void;
};
export const PersonalAddressForm = ({
  address,
  onChange,
}: PersonalAddressProps) => {
  const inputClass = (isValid: boolean) =>
    `form-control p-0 mb-2 ${isValid ? 'is-valid' : 'is-invalid'}`;
  return (
    <div>
      <div className="row mt-3">
        <h6>Personal Address</h6>
      </div>
      <div className="row mt-1">
        <p>
          <small>
            This is the address where you live.
            <br />
            The hotel needs to know this.
          </small>
        </p>
      </div>
      <div className="row mt-3">
        <div className="col-12 col-md-6">
          <InputField
            className={inputClass((address.firstname?.trim() ?? '') !== '')}
            label="First name"
            value={address?.firstname ?? ''}
            placeholder="Your first name"
            required
            onChange={(event) => onChange('firstname', event)}
          />
        </div>
        <div className="col-12 col-md-6">
          <InputField
            className={inputClass((address.lastname?.trim() ?? '') !== '')}
            label="Last name"
            value={address?.lastname ?? ''}
            placeholder="Your last name"
            required
            onChange={(event) => onChange('lastname', event)}
          />
        </div>
      </div>
      <div className="row mt-3">
        <div className="col-12">
          <InputField
            className={inputClass((address.address1?.trim() ?? '') !== '')}
            label="Address 1"
            value={address?.address1 ?? ''}
            placeholder="Address line 1"
            required={true}
            onChange={(event) => onChange('address1', event)}
          />
        </div>
      </div>
      <div className="row mt-3">
        <div className="col-12">
          <InputField
            className="mt-0 mb-2"
            label="Address 2"
            value={address?.address2 ?? ''}
            placeholder="Address line 2"
            onChange={(event) => onChange('address2', event)}
          />
        </div>
      </div>
      <div className="row mt-3">
        <div className="col-12 col-md-6">
          <InputField
            className={inputClass((address.postal?.trim() ?? '') !== '')}
            label="Postal code"
            value={address?.postal ?? ''}
            placeholder="Your postal code"
            required={true}
            onChange={(event) => onChange('postal', event)}
          />
        </div>
        <div className="col-12 col-md-6">
          <InputField
            className={inputClass((address.city?.trim() ?? '') !== '')}
            label="City"
            value={address?.city ?? ''}
            placeholder="Your city"
            required={true}
            onChange={(event) => onChange('city', event)}
          />
        </div>
      </div>
      <div className="row mt-3">
        <div className="col-12">
          <InputField
            className={inputClass((address.country?.trim() ?? '') !== '')}
            label="Country"
            value={address?.country ?? ''}
            placeholder="Country"
            required={true}
            onChange={(event) => onChange('country', event)}
          />
        </div>
      </div>
    </div>
  );
};
