import { Attendee } from '../../main/types';
import React, { ChangeEvent } from 'react';
import { InputField } from '../../form/InputField';

type AccountDataProps = {
  attendee: Attendee;
  onChange: (field: string, event: ChangeEvent<HTMLInputElement>) => void;
};
export const AccountData = ({ attendee, onChange }: AccountDataProps) => {
  return (
    <div>
      <div className={'row'}>
        <h6>Account</h6>
      </div>
      <div className={'row mt-3'}>
        <div className={'col-12'}>
          <InputField
            label={'Nickname'}
            value={attendee.nickname ?? ''}
            placeholder={'Your nickname'}
            onChange={(event) => onChange('nickname', event)}
          />
        </div>
      </div>
      <div className={'row mt-3'}>
        <div className={'col-12'}>
          <InputField
            label={'Email'}
            disabled
            value={attendee.email ?? ''}
            placeholder={'Your email'}
            onChange={(event) => onChange('email', event)}
          />
        </div>
      </div>
    </div>
  );
};
