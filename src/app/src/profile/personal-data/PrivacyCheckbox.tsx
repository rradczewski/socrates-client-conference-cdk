import { InputGroup } from 'react-bootstrap';
import ExternalLink from '../../common/ExternalLink';
import React from 'react';

type PrivacyCheckboxProps = {
  checked: boolean;
  className: string;
  onCheckboxChange: (field: string) => void;
};
export const PrivacyCheckbox = ({
  checked,
  className,
  onCheckboxChange,
}: PrivacyCheckboxProps) => {
  return (
    <div className="row mt-4">
      <div className="col-12 mb-2">
        <InputGroup>
          <InputGroup.Text>
            <input
              type="checkbox"
              aria-label="Checkbox for following text input"
              value="dataPrivacyConfirmed"
              checked={checked}
              onChange={() => onCheckboxChange('privacy')}
            />
          </InputGroup.Text>
          <label className={className} style={{ height: '100%' }}>
            <div className="p-2 pb-0">
              <small>
                <p>
                  I agree that my name and address, as well as my pronoun and
                  social media information, will be collected and processed for
                  the purpose of organizing my participation in the SoCraTes
                  conference, including creating a name badge, booking a hotel
                  room, invoicing and bookkeeping.
                </p>
                <p>
                  Hint: You can revoke your consent at any time until the
                  conference by canceling your participation via the profile
                  page.
                </p>
                <p>
                  Detailed information on handling user data can be found in our{' '}
                  <ExternalLink
                    url="https://socrates-conference.de/privacy-policy"
                    target="_blank"
                    title="privacy policy"
                  >
                    {' '}
                    data privacy policy
                  </ExternalLink>
                </p>
              </small>
            </div>
          </label>
        </InputGroup>
      </div>
    </div>
  );
};
