import { InputField } from '../../form/InputField';
import React, { ChangeEvent } from 'react';
import { Attendee } from '../../main/types';

type BadgeDataProps = {
  attendee: Attendee;
  onChange: (field: string, event: ChangeEvent<HTMLInputElement>) => void;
};

export function BadgeData({ attendee, onChange }: BadgeDataProps) {
  return (
    <div>
      <div className="row mt-3">
        <h6>Name badge</h6>
      </div>
      <div className="row mt-3">
        <div className="col-12">
          <InputField
            label="Badge name"
            value={attendee.badgename ?? ''}
            placeholder="Your badge name"
            required
            onChange={(event) => onChange('badgename', event)}
          />
          <p>
            <small>
              Hint: If you leave this empty, your nickname will appear on your
              badge.
            </small>
          </p>
        </div>
        <div className="col-12">
          <InputField
            label="Pronouns"
            value={attendee.pronouns ?? ''}
            placeholder="Your pronouns"
            required
            onChange={(event) => onChange('pronouns', event)}
          />
          <p>
            <small>
              <a
                href="https://pronouns.org/what-and-why"
                title="Learn about pronouns"
              >
                What is that?
              </a>{' '}
              For example: They/them, she/her, he/him, e/er
            </small>
          </p>
        </div>
        <div className="col-12">
          <InputField
            label="Social media"
            value={attendee.social ?? ''}
            placeholder="Your social media links"
            required
            onChange={(event) => onChange('social', event)}
          />
        </div>
      </div>
    </div>
  );
}
