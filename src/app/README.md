# SoCraTes Conference UI

This React application was created using create-react-app. 

- Download dependencies: `yarn`
- Run tests: `yarn test`
- Run local server: `yarn start`
- Create production build: `yarn build`

## Deploy to AWS
Gitlab will run the CI build and create a deployable application automatically, whenever you push your changes. The rollout itself can be triggered manually from the Gitlab console.
