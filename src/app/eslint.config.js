const parser = require('@typescript-eslint/parser');
const typescript = require('@typescript-eslint/eslint-plugin');
const globals = require('globals');
const reactAppConfig = require('eslint-config-react-app')
const flowtypePlugin = require('eslint-plugin-flowtype')
const jsxA11y = require('eslint-plugin-jsx-a11y')
const reactPlugin = require('eslint-plugin-react')
const reactHooks = require('eslint-plugin-react-hooks')
const importPlugin = require('eslint-plugin-import')
const prettierPlugin = require ('eslint-plugin-prettier')

module.exports = {
  files: ['src/**/*.{js,jsx,ts,tsx}'],
  ignores: [
    'node_modules/',
    'build/',
    'src/assets/**/*',
    'src/**/*.map',
    'src/**/serviceWorker.ts',
  ],
  languageOptions: {
    parser,
    parserOptions: {
      ecmaVersion: 2022,
      ecmaFeatures: {
        jsx: true
      }
    },
    globals: {
      ...globals.browser,
      node: true,
      jest: true
    },
  },
  plugins: {
    '@typescript-eslint': typescript,
    flowtype: flowtypePlugin,
    "jsx-a11y": jsxA11y,
    react: reactPlugin,
    "react-hooks": reactHooks,
    prettier: prettierPlugin,
    import: importPlugin,
    configs: [typescript.configs.recommended, reactAppConfig, prettierPlugin.configs.recommended]
  },
  rules: {
    ...reactAppConfig.rules,
    ...reactAppConfig.overrides[0].rules,
    'prettier/prettier': ['error', {}, { usePrettierrc: true }],
    'react/react-in-jsx-scope': 'off',
    'react/prop-types': 'off',
    '@typescript-eslint/explicit-module-boundary-types': 'off',
    '@typescript-eslint/no-unused-vars': ['warn', { argsIgnorePattern: '^_' }],
    '@typescript-eslint/ban-ts-comment': 'off',
    'react/no-unescaped-entities': 'off',
    '@typescript-eslint/no-explicit-any': 'off',
    '@typescript-eslint/ban-types': 'off',
    '@typescript-eslint/no-var-requires': 'off'
  },
  settings: {
    react: {
      version: "detect"
    }
  }
};
