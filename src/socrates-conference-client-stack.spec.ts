import {SoCraTesConferenceClientStack} from './socrates-conference-client-stack'
import {
  App
} from 'aws-cdk-lib'
import {
  Match,
  Template
} from 'aws-cdk-lib/assertions'

describe('SoCraTesConferenceClientStack:', () => {
  let template: Template
  beforeEach(() => {
    const app = new App()
    const stack = new SoCraTesConferenceClientStack(app, 'SoCraTesConferenceClientStack')
    template = Template.fromStack(stack)
    //console.log(template.toJSON())
  })

  it('deploys a CloudFront Distribution with OAI', () => {
    template.hasResource('AWS::CloudFront::Distribution', {})
    template.hasResource('AWS::CloudFront::CloudFrontOriginAccessIdentity', {})
  })
  it('contains a default route for the app', () => {
    template.hasResourceProperties('AWS::CloudFront::Distribution', {
      DistributionConfig: {
        Origins: [{
          Id: Match.stringLikeRegexp('SoCraTesConferenceClientStackSoCraTesDistributionOrigin.*')
        }]
      }
    })
  })
  it('creates a non-public S3 bucket for the app deployment', () => {
    template.resourcePropertiesCountIs('AWS::S3::Bucket', {
      PublicAccessBlockConfiguration: {
        BlockPublicAcls: true,
        BlockPublicPolicy: true,
        IgnorePublicAcls: true,
        RestrictPublicBuckets: true,
      }
    }, 1)
  })
  it('deploys the app source code', () => {
    const buckets = template.findResources('AWS::S3::Bucket')
    const bucketName: string = Object.keys(buckets)[0]
    template.hasResourceProperties('Custom::CDKBucketDeployment', {
      DestinationBucketName: {
        Ref: bucketName
      }
    })
    template.hasResourceProperties('AWS::Lambda::Function', {
      Layers: [{Ref: Match.stringLikeRegexp('UiDeploymentAwsCliLayer.*')}],
    })
  })
})
