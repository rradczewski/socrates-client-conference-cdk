import * as cdk from 'aws-cdk-lib'
import {RemovalPolicy} from 'aws-cdk-lib'
import {Construct} from 'constructs'
import {
  BlockPublicAccess,
  Bucket,
} from 'aws-cdk-lib/aws-s3'
import {
  BucketDeployment,
  Source
} from 'aws-cdk-lib/aws-s3-deployment'
import * as path from 'node:path'
import {
  Distribution,
  ViewerProtocolPolicy
} from 'aws-cdk-lib/aws-cloudfront'
import {S3Origin} from 'aws-cdk-lib/aws-cloudfront-origins'
import { Certificate } from 'aws-cdk-lib/aws-certificatemanager'

export class SoCraTesConferenceClientStack extends cdk.Stack {
  readonly uiDeploymentBucket: Bucket
  readonly deployment: BucketDeployment
  readonly distribution: Distribution

  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props)


    this.uiDeploymentBucket = new Bucket(this, 'UiDeploymentBucket', {
      blockPublicAccess: BlockPublicAccess.BLOCK_ALL,
      removalPolicy: RemovalPolicy.DESTROY,
      autoDeleteObjects: true
    })


    this.distribution = new Distribution(this, 'SoCraTesDistribution', {
      domainNames: [
        'conf.socrates-conference.de',
        'conf.socrates-conf.de'
      ],
      defaultBehavior: {
        origin: new S3Origin(this.uiDeploymentBucket, {}),
        viewerProtocolPolicy: ViewerProtocolPolicy.REDIRECT_TO_HTTPS
      },
      defaultRootObject: 'index.html',
      errorResponses: [
        {httpStatus: 400, responseHttpStatus: 200, responsePagePath: '/'},
        {httpStatus: 403, responseHttpStatus: 200, responsePagePath: '/'},
        {httpStatus: 404, responseHttpStatus: 200, responsePagePath: '/'},
        {httpStatus: 500, responseHttpStatus: 200, responsePagePath: '/'}
      ],
      certificate: Certificate.fromCertificateArn(this, 'SSLCertificate', 'arn:aws:acm:us-east-1:905418410087:certificate/7e426815-a5f1-48a1-a7f6-5fc2bac87f99')
    })

    this.deployment = new BucketDeployment(this, 'UiDeployment', {
      destinationBucket: this.uiDeploymentBucket,
      sources: [
        Source.asset(path.join(__dirname, 'app/build'))
      ],
      distribution: this.distribution,
      distributionPaths: ['/*']
    })
  }
}
